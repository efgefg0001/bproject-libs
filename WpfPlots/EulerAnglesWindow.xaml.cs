﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OxyPlot;
using QuadcopterMathModel;

namespace WpfPlots
{

    /// <summary>
    /// Interaction logic for PositionsWindow.xaml
    /// </summary>
    public partial class EulerAnglesWindow : Window
    {
        public EulerAnglesWindow(QuadcopterOdeResult result)
        {
            InitializeComponent();
            InitForWork(result);
        }
        
        private IList<DataPoint> GenerateList(int index)
        {
            var t = result.T;
            var x = result.X;
            var count = Math.Min(t.Count, x.Count);
            var list = new List<DataPoint>();
            for (var i = 0; i < count; ++i)
                list.Add(new DataPoint(t[i], MathUtils.FromRadiansToDegrees(x[i][index])));
            return list;
        }

        private QuadcopterOdeResult result;
        private void InitForWork(QuadcopterOdeResult result)
        {
            this.DataContext = this;
            this.result = result;
            PhiT = GenerateList(6);
            ThetaT = GenerateList(8);
            PsiT = GenerateList(10);
        }

        public IList<DataPoint> PhiT { get; private set; }
        public IList<DataPoint> ThetaT { get; private set; }
        public IList<DataPoint> PsiT { get; private set; }
    }
}
