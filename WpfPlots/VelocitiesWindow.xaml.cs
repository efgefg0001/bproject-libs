﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OxyPlot;
using QuadcopterMathModel;

namespace WpfPlots
{

    /// <summary>
    /// Interaction logic for VelocitiesWindow.xaml
    /// </summary>
    public partial class VelocitiesWindow: Window
    {
        public VelocitiesWindow(QuadcopterOdeResult result)
        {
            InitializeComponent();
            InitForWork(result);
        }
        
        private IList<DataPoint> GenerateList(int index)
        {
            var t = result.T;
            var x = result.X;
            var count = Math.Min(t.Count, x.Count);
            var list = new List<DataPoint>();
            for (var i = 0; i < count; ++i)
                list.Add(new DataPoint(t[i], x[i][index]));
            return list;
        }

        private QuadcopterOdeResult result;
        private void InitForWork(QuadcopterOdeResult result)
        {
            this.DataContext = this;
            this.result = result;
            VXT = GenerateList(1);
            VYT = GenerateList(3);
            VZT = GenerateList(5);
        }

        public IList<DataPoint> VXT { get; private set; }
        public IList<DataPoint> VYT { get; private set; }
        public IList<DataPoint> VZT { get; private set; }
    }
}
