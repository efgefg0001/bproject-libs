﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OxyPlot;
using QuadcopterMathModel;

namespace WpfPlots
{

    /// <summary>
    /// Interaction logic for PositionsWindow.xaml
    /// </summary>
    public partial class PositionsWindow : Window
    {
        public PositionsWindow(QuadcopterOdeResult result)
        {
            InitializeComponent();
            InitForWork(result);
        }
        
        private IList<DataPoint> GenerateList(int index)
        {
            var t = result.T;
            var x = result.X;
            var count = Math.Min(t.Count, x.Count);
            var list = new List<DataPoint>();
            for (var i = 0; i < count; ++i)
                list.Add(new DataPoint(t[i], x[i][index]));
            return list;
        }

        private QuadcopterOdeResult result;
        private void InitForWork(QuadcopterOdeResult result)
        {
            this.DataContext = this;
            this.result = result;
            XT = GenerateList(0);
            YT = GenerateList(2);
            ZT = GenerateList(4);
        }

        public IList<DataPoint> XT { get; private set; }
        public IList<DataPoint> YT { get; private set; }
        public IList<DataPoint> ZT { get; private set; }
    }
}
