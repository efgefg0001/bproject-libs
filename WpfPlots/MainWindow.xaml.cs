﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using QuadcopterMathModel;

namespace WpfPlots
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private QuadcoptersFactory factory = new QuadcoptersFactory();

        public QuadcopterParams Quadcopter { get; private set; }
        public TimeParams Time { get; private set; }
        public ObservableCollection<QuadcopterOdeSolver> Solvers { get; private set; }
        public IValidator<QuadcopterOdeSolver> QuadcopterOdeSolverValidator { get; private set; }

        public MainWindow()
        {
            InitializeComponent();
            InitForWork();
        }

        private void InitForWork()
        {
            DataContext = this;
            Quadcopter = factory.CreateUnstableQuadcopter();
                //factory.CreateStableQuadcopter();
            Time = factory.CreateTime();
            Solvers = new ObservableCollection<QuadcopterOdeSolver>()
            {
                factory.CreateFilledQuadcopterOdeSolverWithManualPids(Quadcopter, Time),
                factory.CreateFilledOdeSolverForMamdaniFuzzyPids(Quadcopter, Time),
                factory.CreateFilledOdeSolverForSugenoFuzzyPids(Quadcopter, Time),
                //factory.CreateTestSolver(Time)
            };
            solversComboBox.SelectedValue = Solvers[0];

            QuadcopterOdeSolverValidator = new QuadcopterOdeSolverValidator();
        }
        private QuadcopterOdeSolver solver;

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try {
                solver = solversComboBox.SelectedItem as QuadcopterOdeSolver;
                if (solver != null)
                {
                    //                solver.UsePidRegulation = false;

                    solver.Quadcopter = Quadcopter;
                    solver.Time = Time;

                    QuadcopterOdeSolverValidator.Validate(solver);

                    var resultTask = Task.Run(() => { return solver.Solve(); });

//                    waitWindow.Show();
                    this.IsEnabled = false;

                    ShowResults(await resultTask);

                }
            } catch (Exception exception)
            {
                this.IsEnabled = true;
//                waitWindow.Close();

                MessageBox.Show(exception.Message);
            }
        }
        private void ShowResults(QuadcopterOdeResult result)
        {
            this.IsEnabled = true;
//            waitWindow.Close();

            var screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            var width = 0.45 * screenWidth;
            var screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            var height = 0.45 * screenHeight;
            var positionsWindow = new PositionsWindow(result);

            positionsWindow.Width = width; positionsWindow.Height = height;
            positionsWindow.Show();

/*
            var velocitiesWindow = new VelocitiesWindow(result);
            velocitiesWindow.Width = width; velocitiesWindow.Height = height;
            velocitiesWindow.Left = screenWidth - velocitiesWindow.Width;
            velocitiesWindow.Show();
*/
            var pidsWindow = new PidsWindow(
                string.Format("ZPid: Kp = {0}, Ki = {1}, Kd = {2}\n", solver.ZPid.Kp, solver.ZPid.Ki, solver.ZPid.Kd) +
                string.Format("PhiPid: Kp = {0}, Ki = {1}, Kd = {2}\n", solver.PhiPid.Kp, solver.PhiPid.Ki, solver.PhiPid.Kd) +
                string.Format("ThetaPid: Kp = {0}, Ki = {1}, Kd = {2}\n", solver.ThetaPid.Kp, solver.ThetaPid.Ki, solver.ThetaPid.Kd) +
                string.Format("PsiPid: Kp = {0}, Ki = {1}, Kd = {2}\n", solver.PsiPid.Kp, solver.PsiPid.Ki, solver.PsiPid.Kd)
            );
            pidsWindow.Width = width; pidsWindow.Height = height;
            pidsWindow.Left = screenWidth - pidsWindow.Width;
            pidsWindow.Show();

            var angularVelocitiesWindow = new AngularVelocitiesWindow(result);
            angularVelocitiesWindow.Width = width; angularVelocitiesWindow.Height = height;
            angularVelocitiesWindow.Top = screenHeight - angularVelocitiesWindow.Height;
            angularVelocitiesWindow.Show();

            var eulerAnglesWindow = new EulerAnglesWindow(result);
            eulerAnglesWindow.Width = width; eulerAnglesWindow.Height = height;
            eulerAnglesWindow.Top = screenHeight - eulerAnglesWindow.Height;
            eulerAnglesWindow.Left = screenWidth - eulerAnglesWindow.Width;
            eulerAnglesWindow.Show();


        }

    }
}
