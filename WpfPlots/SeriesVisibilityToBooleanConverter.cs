﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OxyPlot.Series;
using System.Windows.Data;
using System.Globalization;

namespace WpfPlots
{
    public class SeriesVisibilityToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = false;
            if (value.ToString() == "Visible")
                result = true;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = "Hidden";
            if (value.ToString() == "True")
                result = "Visible";
            return result;
        }
    }
}
