﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OxyPlot;
using QuadcopterMathModel;

namespace WpfPlots
{

    /// <summary>
    /// Interaction logic for AngularVelocitiesWindow.xaml
    /// </summary>
    public partial class AngularVelocitiesWindow: Window
    {
        public AngularVelocitiesWindow(QuadcopterOdeResult result)
        {
            InitializeComponent();
            InitForWork(result);
        }
        
        private IList<DataPoint> GenerateList(int index)
        {
            var t = result.T;
            var omega = result.Omega;
            var count = Math.Min(t.Count, omega.Count);
            var list = new List<DataPoint>();
            for (var i = 0; i < count; ++i)
                list.Add(new DataPoint(t[i], omega[i][index]));
            return list;
        }

        private QuadcopterOdeResult result;
        private void InitForWork(QuadcopterOdeResult result)
        {
            this.DataContext = this;
            this.result = result;
            OmegaOne = GenerateList(0);
            OmegaTwo = GenerateList(1);
            OmegaThree = GenerateList(2);
            OmegaFour = GenerateList(3);
        }

        public IList<DataPoint> OmegaOne { get; private set; }
        public IList<DataPoint> OmegaTwo { get; private set; }
        public IList<DataPoint> OmegaThree { get; private set; }
        public IList<DataPoint> OmegaFour { get; private set; }
    }
}
