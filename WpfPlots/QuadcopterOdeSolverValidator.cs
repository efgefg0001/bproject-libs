﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using QuadcopterMathModel;

namespace WpfPlots
{
    class QuadcopterOdeSolverValidator : IValidator<QuadcopterOdeSolver>
    {
        private void ValidateOmegas(QuadcopterParams quadcopter)
        {
            if (quadcopter.Motor1.StartOmega < quadcopter.Motor1.MinOmega)
                throw new Exception("ω1: Угловая скорость должна быть >= " + quadcopter.Motor1.MinOmega);
            if (quadcopter.Motor1.StartOmega > quadcopter.Motor1.MaxOmega)
                throw new Exception("ω1: Угловая скорость должна быть <= " + quadcopter.Motor1.MaxOmega);

            if (quadcopter.Motor2.StartOmega < quadcopter.Motor2.MinOmega)
                throw new Exception("ω2: Угловая скорость должна быть >= " + quadcopter.Motor2.MinOmega);
            if (quadcopter.Motor2.StartOmega > quadcopter.Motor2.MaxOmega)
                throw new Exception("ω2: Угловая скорость должна быть <= " + quadcopter.Motor2.MaxOmega);

            if (quadcopter.Motor3.StartOmega < quadcopter.Motor3.MinOmega)
                throw new Exception("ω3: Угловая скорость должна быть >= " + quadcopter.Motor3.MinOmega);
            if (quadcopter.Motor3.StartOmega > quadcopter.Motor3.MaxOmega)
                throw new Exception("ω3: Угловая скорость должна быть <= " + quadcopter.Motor3.MaxOmega);

            if (quadcopter.Motor4.StartOmega < quadcopter.Motor4.MinOmega)
                throw new Exception("ω4: Угловая скорость должна быть >= " + quadcopter.Motor4.MinOmega);
            if (quadcopter.Motor4.StartOmega > quadcopter.Motor4.MaxOmega)
                throw new Exception("ω4: Угловая скорость должна быть <= " + quadcopter.Motor4.MaxOmega);
        }
        
        private void ThrowIfNotBetweenMinMax(double num, double min, double max, string message)
        {
            if (num < min || num > max)
                throw new Exception(message);
        }

        private readonly double deltaZMin = -5, deltaZMax = 5;
        private readonly double deltaPhiMinDegrees = -45, deltaPhiMaxDegrees = 45;
        private readonly double deltaThetaMinDegrees = -45, deltaThetaMaxDegrees = 45;
        private readonly double deltaPsiMinDegrees = -45, deltaPsiMaxDegrees = 45;

        private void ValidateZ(QuadcopterParams quadcopter)
        {
            var deltaZ = quadcopter.Zdest - quadcopter.StartPosition.Z;
            ThrowIfNotBetweenMinMax(deltaZ, deltaZMin, deltaZMax, 
                string.Format("Разность между целевым z и начальным z должна находиться в [{0}, {1}]", deltaZMin, deltaZMax)
            );
        }

        private void ValidateEulerAngles(QuadcopterParams quadcopter)
        {
            var deltaPhi = quadcopter.DestAttitude.PhiDegrees - quadcopter.StartAttitude.PhiDegrees;
            ThrowIfNotBetweenMinMax(deltaPhi, deltaPhiMinDegrees, deltaPhiMaxDegrees, 
                string.Format("Разность между целевым значением крена и начальным значением крена должна находиться в [{0}, {1}]", deltaPhiMinDegrees, deltaPhiMaxDegrees)
            );

            var deltaTheta = quadcopter.DestAttitude.ThetaDegrees - quadcopter.StartAttitude.ThetaDegrees;
            ThrowIfNotBetweenMinMax(deltaTheta, deltaThetaMinDegrees, deltaThetaMaxDegrees, 
                string.Format("Разность между целевым значением тангажа и начальным значением тангажа должна находиться в [{0}, {1}]", deltaThetaMinDegrees, deltaThetaMaxDegrees)
            );

            var deltaPsi = quadcopter.DestAttitude.PsiDegrees - quadcopter.StartAttitude.PsiDegrees;
            ThrowIfNotBetweenMinMax(deltaPsi, deltaPsiMinDegrees, deltaPsiMaxDegrees, 
                string.Format("Разность между целевым значением рыскания и начальным значением рыскания должна находиться в [{0}, {1}]", deltaPsiMinDegrees, deltaPsiMaxDegrees)
            );
        }

        public void Validate(QuadcopterOdeSolver solver)
        {
            var quadcopter = solver.Quadcopter;
            ValidateOmegas(quadcopter);
            ValidateZ(quadcopter);
            ValidateEulerAngles(quadcopter);
        }
    }
}
