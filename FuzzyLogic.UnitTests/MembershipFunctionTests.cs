﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace FuzzyLogic.UnitTests
{
    [TestFixture]
    class TriangularMembershipFunctionTests
    {
        private MembershipFunction CreateMembFunc()
        {
            return new TriangularMembershipFunction(10, 40, 55);
        }

        [TestCase]
        public void GetValue_LessThanLeftBoundary_ZeroValue()
        {
            var trMembFunc = CreateMembFunc(); 
            var val = trMembFunc.GetValue(9);
            Assert.IsTrue(Math.Abs(val - 0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_EqualsLeftBoundary_ZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(10);
            Assert.IsTrue(Math.Abs(val - 0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_BetweenX1AndX2_NoneZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(20);
            Assert.IsTrue(Math.Abs(val - 1.0/3.0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_XequalsX2_One()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(40);
            Assert.IsTrue(Math.Abs(val - 1.0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_BetweenX2AndX3_NoneZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(50);
            Assert.IsTrue(Math.Abs(val - 1.0 / 3.0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_EqualsRightBoundary_ZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(55);
            Assert.IsTrue(Math.Abs(val - 0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_GreaterThanRightBoundary_ZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(56);
            Assert.IsTrue(Math.Abs(val - 0) < trMembFunc.EPS);
        }
    }

    [TestFixture]
    class TrapezoidMembershipFunctionTests
    {
        private MembershipFunction CreateMembFunc()
        {
            return new TrapezoidMembershipFunction(10, 40, 60, 70);
        }

        [TestCase]
        public void GetValue_LessThanLeftBoundary_ZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(9);
            Assert.IsTrue(Math.Abs(val - 0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_EqualsLeftBoundary_ZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(10);
            Assert.IsTrue(Math.Abs(val - 0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_BetweenX1AndX2_NoneZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(20);
            Assert.IsTrue(Math.Abs(val - 1.0/3.0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_XequalsX2_One()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(40);
            Assert.IsTrue(Math.Abs(val - 1.0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_BetweenX2AndX3_One()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(50);
            Assert.IsTrue(Math.Abs(val - 1.0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_XequalsX3_One()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(60);
            Assert.IsTrue(Math.Abs(val - 1.0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_BetweenX3AndX4_BetweenOneAndZero()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(65);
            Assert.IsTrue(Math.Abs(val - 0.5) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_EqualsRightBoundary_ZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(70);
            Assert.IsTrue(Math.Abs(val - 0) < trMembFunc.EPS);
        }

        [TestCase]
        public void GetValue_GreaterThanRightBoundary_ZeroValue()
        {
            var trMembFunc = CreateMembFunc();
            var val = trMembFunc.GetValue(80);
            Assert.IsTrue(Math.Abs(val - 0) < trMembFunc.EPS);
        }
    }
}
