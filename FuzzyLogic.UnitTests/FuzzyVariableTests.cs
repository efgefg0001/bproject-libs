﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace FuzzyLogic.UnitTests
{
    [TestFixture]
    class FuzzyVariableTests
    {
        [TestCase]
        public void Constructor_CreationOfFuzzyVarWithoutTerms_Success()
        {
            var fuzzyVar = new FuzzyVariable("velocity", 0, 150);

            Assert.AreEqual("velocity", fuzzyVar.Name);
            Assert.AreEqual(0, fuzzyVar.Min);
            Assert.AreEqual(150, fuzzyVar.Max);
        }

        [TestCase]
        public void GetTermByName_ExistingTerm_Success()
        {
            var fuzzyVar = new FuzzyVariable("velocity", 0, 150);
            var lowVelocityTerm = new FuzzyTerm("low", new TrapezoidMembershipFunction(0, 0, 30, 50));
            var avgVelocityTerm = new FuzzyTerm("averrage", new TrapezoidMembershipFunction(55, 70, 100, 105));
            var highVelocityTerm = new FuzzyTerm("high", new TrapezoidMembershipFunction(110, 120, 150, 150));
            fuzzyVar.Terms.Add(lowVelocityTerm);
            fuzzyVar.Terms.Add(avgVelocityTerm);
            fuzzyVar.Terms.Add(highVelocityTerm);
            Assert.AreEqual(lowVelocityTerm, fuzzyVar.GetTermByName("low"));
            Assert.AreEqual(avgVelocityTerm, fuzzyVar.GetTermByName("averrage"));
            Assert.AreEqual(highVelocityTerm, fuzzyVar.GetTermByName("high"));
        }
    }
}
