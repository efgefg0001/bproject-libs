﻿using System;
using System.Collections.Generic;

using NUnit.Framework;
using FuzzyLogic;

namespace FuzzyLogic.UnitTests
{
    [TestFixture]
    class SugenoFuzzySystemTests
    {
        private const double EPS = 1e-3;

        private void AddSugenoFuzzyRule(
            SugenoFuzzySystem fs,
            FuzzyVariable fv1,
            FuzzyVariable fv2,
            SugenoVariable sv,
            string value1,
            string value2,
            string result)
        {
            SugenoFuzzyRule rule = fs.EmptyRule();
            rule.Condition.Op = OperatorType.And;
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv1, fv1.GetTermByName(value1)));
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv2, fv2.GetTermByName(value2)));
            rule.Conclusion.Var = sv;
            rule.Conclusion.Term = sv.GetFuncByName(result);
            fs.Rules.Add(rule);
        }
        private SugenoFuzzySystem CreateSpeedErrorSugenoFuzzySystem()
        {
            // Создать пустую систему нечёткого вывода Сугено
            SugenoFuzzySystem fsCruiseControl = new SugenoFuzzySystem();

            // Создать входные переменные для системы
            FuzzyVariable fvSpeedError = new FuzzyVariable("SpeedError", -20.0, 20.0);
            fvSpeedError.Terms.Add(new FuzzyTerm("slower", new TriangularMembershipFunction(-35.0, -20.0, -5.0)));
            fvSpeedError.Terms.Add(new FuzzyTerm("zero", new TriangularMembershipFunction(-15.0, -0.0, 15.0)));
            fvSpeedError.Terms.Add(new FuzzyTerm("faster", new TriangularMembershipFunction(5.0, 20.0, 35.0)));
            fsCruiseControl.Input.Add(fvSpeedError);

            FuzzyVariable fvSpeedErrorDot = new FuzzyVariable("SpeedErrorDot", -5.0, 5.0);
            fvSpeedErrorDot.Terms.Add(new FuzzyTerm("slower", new TriangularMembershipFunction(-9.0, -5.0, -1.0)));
            fvSpeedErrorDot.Terms.Add(new FuzzyTerm("zero", new TriangularMembershipFunction(-4.0, -0.0, 4.0)));
            fvSpeedErrorDot.Terms.Add(new FuzzyTerm("faster", new TriangularMembershipFunction(1.0, 5.0, 9.0)));
            fsCruiseControl.Input.Add(fvSpeedErrorDot);

            // Создать выходные переменные для системы
            SugenoVariable svAccelerate = new SugenoVariable("Accelerate");
            svAccelerate.Functions.Add(fsCruiseControl.CreateSugenoFunction("zero", new double[] { 0.0, 0.0, 0.0 }));
            svAccelerate.Functions.Add(fsCruiseControl.CreateSugenoFunction("faster", new double[] { 0.0, 0.0, 1.0 }));
            svAccelerate.Functions.Add(fsCruiseControl.CreateSugenoFunction("slower", new double[] { 0.0, 0.0, -1.0 }));
            svAccelerate.Functions.Add(fsCruiseControl.CreateSugenoFunction("func", new double[] { -0.04, -0.1, 0.0 }));
            fsCruiseControl.Output.Add(svAccelerate);

            // Создать нечёткие правила
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "slower", "slower", "faster");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "slower", "zero", "faster");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "slower", "faster", "zero");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "zero", "slower", "faster");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "zero", "zero", "func");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "zero", "faster", "slower");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "faster", "slower", "zero");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "faster", "zero", "slower");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "faster", "faster", "slower");

            return fsCruiseControl;
        }

        [TestCase]
        public void Calculate_SpeedErrorSugenoFuzzySystem_EqSettedValue()
        {
            var fsCruiseControl = CreateSpeedErrorSugenoFuzzySystem();
            // Получить переменные из системы
            FuzzyVariable fvSpeedError = fsCruiseControl.InputByName("SpeedError");
            FuzzyVariable fvSpeedErrorDot = fsCruiseControl.InputByName("SpeedErrorDot");
            SugenoVariable svAccelerate = fsCruiseControl.OutputByName("Accelerate");
            // Фуззифицировать входные значения
            Dictionary<FuzzyVariable, double> inputValues = new Dictionary<FuzzyVariable, double>();
            inputValues.Add(fvSpeedError, 10);
            inputValues.Add(fvSpeedErrorDot, -4);

            // Вычислить результат
            Dictionary<SugenoVariable, double> result = fsCruiseControl.Calculate(inputValues);
            var resultVal = result[svAccelerate];

            Assert.AreEqual(0.5, resultVal, EPS);
        }
    }
}
