﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using FuzzyLogic;

namespace FuzzyLogic.UnitTests
{
    [TestFixture]
    class MamdaniFuzzySystemTests
    {
        private const double EPS = 1e-3;

        private MamdaniFuzzySystem CreateSimpleMamdaniFuzzySystem()
        {
            // Создать пустую систему Мамдани
            var fsTips = new MamdaniFuzzySystem();

            // Создать входные переменные для сиситемы
            FuzzyVariable fvService = new FuzzyVariable("service", 0.0, 10.0);
            fvService.Terms.Add(new FuzzyTerm("poor", new TriangularMembershipFunction(-5.0, 0.0, 5.0)));
            fvService.Terms.Add(new FuzzyTerm("good", new TriangularMembershipFunction(0.0, 5.0, 10.0)));
            fvService.Terms.Add(new FuzzyTerm("excellent", new TriangularMembershipFunction(5.0, 10.0, 15.0)));
            fsTips.Input.Add(fvService);

            FuzzyVariable fvFood = new FuzzyVariable("food", 0.0, 10.0);
            fvFood.Terms.Add(new FuzzyTerm("rancid", new TrapezoidMembershipFunction(0.0, 0.0, 1.0, 3.0)));
            fvFood.Terms.Add(new FuzzyTerm("delicious", new TrapezoidMembershipFunction(7.0, 9.0, 10.0, 10.0)));
            fsTips.Input.Add(fvFood);

            // Создать выходные переменные для системы
            FuzzyVariable fvTips = new FuzzyVariable("tips", 0.0, 30.0);
            fvTips.Terms.Add(new FuzzyTerm("cheap", new TriangularMembershipFunction(0.0, 5.0, 10.0)));
            fvTips.Terms.Add(new FuzzyTerm("average", new TriangularMembershipFunction(10.0, 15.0, 20.0)));
            fvTips.Terms.Add(new FuzzyTerm("generous", new TriangularMembershipFunction(20.0, 25.0, 30.0)));
            fsTips.Output.Add(fvTips);

            // Создать правила
            MamdaniFuzzyRule rule1 = fsTips.ParseRule("if (service is poor)  or (food is rancid) then tips is cheap");
            MamdaniFuzzyRule rule2 = fsTips.ParseRule("if (service is good) then tips is average");
            MamdaniFuzzyRule rule3 = fsTips.ParseRule("if (service is excellent) or (food is delicious) then (tips is generous)");
            fsTips.Rules.Add(rule1);
            fsTips.Rules.Add(rule2);
            fsTips.Rules.Add(rule3);
            return fsTips;
        }

        [TestCase]
        public void Calculate_MamdSysWith2InputVarsAnd1Output_Success()
        {
            var fsTips = CreateSimpleMamdaniFuzzySystem();
            FuzzyVariable fvService = fsTips.InputByName("service");
            FuzzyVariable fvFood = fsTips.InputByName("food");
            FuzzyVariable fvTips = fsTips.OutputByName("tips");

            // Входные значения с входными переменными
            Dictionary<FuzzyVariable, double> inputValues = new Dictionary<FuzzyVariable, double>();
            inputValues.Add(fvService, 0.0);
            inputValues.Add(fvFood, 0.0);

            // Вычислить результат
            Dictionary<FuzzyVariable, double> result = fsTips.Calculate(inputValues);

            Assert.AreEqual(5.0001, result[fvTips], EPS);
        }
    }
}
