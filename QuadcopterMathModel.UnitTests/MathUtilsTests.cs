﻿using System;
using NUnit.Framework;

using QuadcopterMathModel;

namespace QuadcopterMathModel.UnitTests
{
    [TestFixture]
    public class MathUtilsTests
    {

        [TestCase]
        public void TrapezoidIntegration_Xfunc_CorrectIntegral()
        {
            Func<double, double> func = (t) => t;
            double min = 0, max = 4;

            var result = MathUtils.TrapezoidIntegration(func, min, max);

            Assert.AreEqual(8, result);
        }

        [TestCase]
        public void TrapezoidIntegration_X2func_CorrectIntegral()
        {
            Func<double, double> func = (t) => t * t;
            double min = 0, max = 4;

            var result = MathUtils.TrapezoidIntegration(func, min, max);

            Assert.AreEqual(4.0 * 4.0 * 4.0 / 3, result, 0.2);
        }
    }
}
