﻿using System;
using System.Collections.Generic;


namespace FuzzyLogic
{
    /// <summary>
    /// Мамдани
    /// </summary>
    public class MamdaniFuzzySystem : GenericFuzzySystem
    {
        private List<FuzzyVariable> _output = new List<FuzzyVariable>();
        private List<MamdaniFuzzyRule> _rules = new List<MamdaniFuzzyRule>();

        private ImplicationMethod _implMethod = ImplicationMethod.Min;
        private AggregationMethod _aggrMethod = AggregationMethod.Max;
        private DefuzzificationMethod _defuzzMethod = DefuzzificationMethod.Centroid;

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public MamdaniFuzzySystem()
        {
        }

        /// <summary>
        /// Выходные лингвистические переменные
        /// </summary>
        public List<FuzzyVariable> Output
        {
            get { return _output; }
        }

        /// <summary>
        /// Нечёткие правила
        /// </summary>
        public List<MamdaniFuzzyRule> Rules
        {
            get { return _rules; }
        }

        /// <summary>
        /// Метод логического вывода
        /// </summary>
        public ImplicationMethod ImplicationMethod
        {
            get { return _implMethod; }
            set { _implMethod = value; }
        }

        /// <summary>
        /// Метод аггрегации
        /// </summary>
        public AggregationMethod AggregationMethod
        {
            get { return _aggrMethod; }
            set { _aggrMethod = value; }
        }

        /// <summary>
        /// Метод дефуззификации
        /// </summary>
        public DefuzzificationMethod DefuzzificationMethod
        {
            get { return _defuzzMethod; }
            set { _defuzzMethod = value; }
        }

        /// <summary>
        /// Получить выходную лингвистическую переменную по имени
        /// </summary>
        /// <param name="name">Имя переменной</param>
        /// <returns>Найденная переменная</returns>
        public FuzzyVariable OutputByName(string name)
        {
            foreach (FuzzyVariable var in _output)
            {
                if (var.Name == name)
                {
                    return var;
                }
            }

            throw new KeyNotFoundException();
        }

        /// <summary>
        /// Создать новое пустое правило
        /// </summary>
        /// <returns></returns>
        public MamdaniFuzzyRule EmptyRule()
        {
            return new MamdaniFuzzyRule();
        }

        /// <summary>
        /// Разобрать правило по строке
        /// </summary>
        /// <param name="rule">Строка, содержащая правило</param>
        /// <returns></returns>
        public MamdaniFuzzyRule ParseRule(string rule)
        {
            return RuleParser<MamdaniFuzzyRule, FuzzyVariable, FuzzyTerm>.Parse(rule, EmptyRule(), Input, Output);
        }

        /// <summary>
        /// Вычислить выходные значение
        /// </summary>
        /// <param name="inputValues">Входные значения (фомат: переменная - значение)</param>
        /// <returns>Выходные значения (формат: переменная - значение)</returns>
        public Dictionary<FuzzyVariable, double> Calculate(Dictionary<FuzzyVariable, double> inputValues)
        {
            // Здесь должно быть как минимум одно правило
            if (_rules.Count == 0)
            {
                throw new Exception("There should be one rule as minimum.");
            }

            // Шаг фуззификации
            Dictionary<FuzzyVariable, Dictionary<FuzzyTerm, double>> fuzzifiedInput =
                Fuzzify(inputValues);

            // Вычислить условия
            Dictionary<MamdaniFuzzyRule, double> evaluatedConditions = EvaluateConditions(fuzzifiedInput);

            // Выполнить импликацию для каждого правила
            Dictionary<MamdaniFuzzyRule, MembershipFunction> implicatedConclusions = Implicate(evaluatedConditions);

            // Аггрегировать результаты
            Dictionary<FuzzyVariable, MembershipFunction> fuzzyResult = Aggregate(implicatedConclusions);

            // Дефуззифицировать результат
            Dictionary<FuzzyVariable, double> result = Defuzzify(fuzzyResult);

            return result;
        }

        /// <summary>
        /// Вычислить условия
        /// </summary>
        /// <param name="fuzzifiedInput">Вход в фуззифицированной форме</param>
        /// <returns>Результат вычисления</returns>
        public Dictionary<MamdaniFuzzyRule, double> EvaluateConditions(Dictionary<FuzzyVariable, Dictionary<FuzzyTerm, double>> fuzzifiedInput)
        {
            Dictionary<MamdaniFuzzyRule, double> result = new Dictionary<MamdaniFuzzyRule,double>();
            foreach (MamdaniFuzzyRule rule in Rules)
            {
                result.Add(rule, EvaluateCondition(rule.Condition, fuzzifiedInput));
            }

            return result;
        }

        /// <summary>
        /// Имплицировать результаты правила
        /// </summary>
        /// <param name="conditions">Условия правила</param>
        /// <returns>Имплицированное заключение</returns>
        public Dictionary<MamdaniFuzzyRule, MembershipFunction> Implicate(Dictionary<MamdaniFuzzyRule, double> conditions)
        {
            Dictionary<MamdaniFuzzyRule, MembershipFunction> conclusions = new Dictionary<MamdaniFuzzyRule, MembershipFunction>();
            foreach (MamdaniFuzzyRule rule in conditions.Keys)
            {
                MfCompositionType compType;
                switch (_implMethod)
                {
                    case ImplicationMethod.Min:
                        compType = MfCompositionType.Min;
                        break;
                    case ImplicationMethod.Production:
                        compType = MfCompositionType.Prod;
                        break;
                    default:
                        throw new Exception("Internal error.");
                }

                CompositeMembershipFunction resultMf = new CompositeMembershipFunction(
                    compType,
                    new ConstantMembershipFunction(conditions[rule]),
                    ((FuzzyTerm)rule.Conclusion.Term).MembershipFunction);
                conclusions.Add(rule, resultMf);
            }

            return conclusions;
        }

        
        /// <summary>
        /// Результаты аггрегирования
        /// </summary>
        /// <param name="conclusions">Результаты правил</param>
        /// <returns>Аггрегированный нечёткий результат</returns>
        public Dictionary<FuzzyVariable, MembershipFunction> Aggregate(Dictionary<MamdaniFuzzyRule, MembershipFunction> conclusions)
        {
            Dictionary<FuzzyVariable, MembershipFunction> fuzzyResult = new Dictionary<FuzzyVariable, MembershipFunction>();
            foreach (FuzzyVariable var in Output)
            {
                List<MembershipFunction> mfList = new List<MembershipFunction>();
                foreach (MamdaniFuzzyRule rule in conclusions.Keys)
                {
                    if (rule.Conclusion.Var == var)
                    {
                        mfList.Add(conclusions[rule]);
                    }
                }

                MfCompositionType composType;
                switch (_aggrMethod)
                {
                    case AggregationMethod.Max:
                        composType = MfCompositionType.Max;
                        break;
                    case AggregationMethod.Sum:
                        composType = MfCompositionType.Sum;
                        break;
                    default:
                        throw new Exception("Internal exception.");
                }
                fuzzyResult.Add(var, new CompositeMembershipFunction(composType, mfList));
            }

            return fuzzyResult;
        }

        /// <summary>
        /// Вычислить чёткий результат для каждого правила
        /// </summary>
        /// <param name="fuzzyResult"></param>
        /// <returns></returns>
        public Dictionary<FuzzyVariable, double> Defuzzify(Dictionary<FuzzyVariable, MembershipFunction> fuzzyResult)
        {
            Dictionary<FuzzyVariable, double> crispResult = new Dictionary<FuzzyVariable, double>();
            foreach (FuzzyVariable var in fuzzyResult.Keys)
            {
                crispResult.Add(var, Defuzzify(fuzzyResult[var], var.Min, var.Max));
            }

            return crispResult;
        }

        double Defuzzify(MembershipFunction mf, double min, double max)
        {
            if (_defuzzMethod == DefuzzificationMethod.Centroid)
            {
                int k = 50;
                double step = (max - min) / k;

                // Вычислить центр тяжести как интеграл
                double ptLeft = 0.0;
                double ptCenter = 0.0;
                double ptRight = 0.0;

                double valLeft = 0.0;
                double valCenter = 0.0;
                double valRight = 0.0;

                double val2Left = 0.0;
                double val2Center = 0.0;
                double val2Right = 0.0;

                double numerator = 0.0;
                double denominator = 0.0;
                for (int i = 0; i < k; i++)
                {
                    if (i == 0)
                    {
                        ptRight = min;
                        valRight = mf.GetValue(ptRight);
                        val2Right = ptRight * valRight;
                    }

                    ptLeft = ptRight;
                    ptCenter = min + step * ((double)i + 0.5);
                    ptRight = min + step * (i + 1);

                    valLeft = valRight;
                    valCenter = mf.GetValue(ptCenter);
                    valRight = mf.GetValue(ptRight);

                    val2Left = val2Right;
                    val2Center = ptCenter * valCenter;
                    val2Right = ptRight * valRight;

                    numerator += step * (val2Left + 4 * val2Center + val2Right) / 3.0;
                    denominator += step * (valLeft + 4 * valCenter + valRight) / 3.0;
                }

                return numerator / denominator;
            }
            else if (_defuzzMethod == DefuzzificationMethod.Bisector)
            {
                // TODO:
                throw new NotSupportedException();
            }
            else if (_defuzzMethod == DefuzzificationMethod.AverageMaximum)
            {
                // TODO:
                throw new NotSupportedException();
            }
            else
            {
                throw new Exception("Internal exception.");
            }
        }
    }
}
