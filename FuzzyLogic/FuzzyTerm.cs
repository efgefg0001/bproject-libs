﻿using System;
using System.Collections.Generic;


namespace FuzzyLogic
{

    /// <summary>
    /// Терм
    /// </summary>
    public class FuzzyTerm : NamedValueImpl
    {
        MembershipFunction _mf;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя терма</param>
        /// <param name="mf">Функция принадлежности</param>
        public FuzzyTerm(string name, MembershipFunction mf) : base(name)
        {
            _mf = mf;
        }

        /// <summary>
        /// Функция принадлежности
        /// </summary>
        public MembershipFunction MembershipFunction
        {
            get { return _mf; }
        }
    }
}
