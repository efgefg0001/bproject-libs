﻿using System;
using System.Collections.Generic;


namespace FuzzyLogic
{

    /// <summary>
    /// Система нечёткого вывода Сугено
    /// </summary>
    public class SugenoFuzzySystem : GenericFuzzySystem
    {
        List<SugenoVariable> _output = new List<SugenoVariable>();
        List<SugenoFuzzyRule> _rules = new List<SugenoFuzzyRule>();            

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public SugenoFuzzySystem()
        {}

        /// <summary>
        /// Вывод системы
        /// </summary>
        public List<SugenoVariable> Output
        {
            get { return _output; }
        }

        /// <summary>
        /// Список правил системы
        /// </summary>
        public List<SugenoFuzzyRule> Rules
        {
            get { return _rules; }
        }

        /// <summary>
        /// Получить выходную переменную системы по имени
        /// </summary>
        /// <param name="name">Имя переменной</param>
        /// <returns>Найденная переменная</returns>
        public SugenoVariable OutputByName(string name)
        {
            foreach (SugenoVariable var in _output)
            {
                if (var.Name == name)
                {
                    return var;
                }
            }

            throw new KeyNotFoundException();
        }

        /// <summary>
        /// Использовать этот метод, чтобы создать линейную функцию для линейной функции для Сугено
        /// </summary>
        /// <param name="name">Имя функции</param>
        /// <param name="coeffs">Список коэффициентов. Длина списка меньше или равна длине входа.</param>
        /// <param name="constValue"></param>
        /// <returns>Созданная функция</returns>
        public LinearSugenoFunction CreateSugenoFunction(string name, Dictionary<FuzzyVariable, double> coeffs, double constValue)
        {
            return new LinearSugenoFunction(name, this.Input, coeffs, constValue);
        }

        /// <summary>
        /// Использовать этот метод, чтобы создать линейную функцию для Сугено
        /// </summary>
        /// <param name="name">Имя функции</param>
        /// <param name="coeffs">Список коэффициентов. Длина списка меньше или равна длине входа.</param>
        /// <returns>Созданная функция</returns>
        public LinearSugenoFunction CreateSugenoFunction(string name, double[] coeffs)
        {
            return new LinearSugenoFunction(name, this.Input, coeffs);
        }

        /// <summary>
        /// Использовать этот метод, чтобы создать пустое правило для системы
        /// </summary>
        /// <returns>Созданное правило</returns>
        public SugenoFuzzyRule EmptyRule()
        {
            return new SugenoFuzzyRule();
        }

        /// <summary>
        /// Использовать этот метод, чтобы создать правило по его текстовому представлению
        /// </summary>
        /// <param name="rule">Правило в текстовой форме</param>
        /// <returns>Созданное правило</returns>
        public SugenoFuzzyRule ParseRule(string rule)
        {
            return RuleParser<SugenoFuzzyRule, SugenoVariable, ISugenoFunction>.Parse(rule, EmptyRule(), Input, Output);
        }

        /// <summary>
        /// Вычислить условия
        /// </summary>
        /// <param name="fuzzifiedInput">Вход в фуззифицированной форме</param>
        /// <returns>Результат вычисления</returns>
        public Dictionary<SugenoFuzzyRule, double> EvaluateConditions(Dictionary<FuzzyVariable, Dictionary<FuzzyTerm, double>> fuzzifiedInput)
        {
            Dictionary<SugenoFuzzyRule, double> result = new Dictionary<SugenoFuzzyRule, double>();
            foreach (SugenoFuzzyRule rule in Rules)
            {
                result.Add(rule, EvaluateCondition(rule.Condition, fuzzifiedInput));
            }

            return result;
        }

        /// <summary>
        /// Вычислить результаты функций
        /// </summary>
        /// <param name="inputValues">Входные значения</param>
        /// <returns>Результаты</returns>
        public Dictionary<SugenoVariable, Dictionary<ISugenoFunction, double>> EvaluateFunctions(Dictionary<FuzzyVariable, double> inputValues)
        {
            Dictionary<SugenoVariable, Dictionary<ISugenoFunction, double>> result = new Dictionary<SugenoVariable, Dictionary<ISugenoFunction, double>>();

            foreach (SugenoVariable var in Output)
            {
                Dictionary<ISugenoFunction, double> varResult = new Dictionary<ISugenoFunction, double>();

                foreach (ISugenoFunction func in var.Functions)
                {
                    varResult.Add(func, func.Evaluate(inputValues));
                }

                result.Add(var, varResult);
            }

            return result;
        }

        /// <summary>
        /// Объединить результаты функций и правила вычисления
        /// </summary>
        /// <param name="ruleWeights">Веса правила (результаты вычисления)</param>
        /// <param name="functionResults">Результат вычисления функций</param>
        /// <returns>Результат вычислений</returns>
        public Dictionary<SugenoVariable, double> CombineResult(Dictionary<SugenoFuzzyRule, double> ruleWeights, Dictionary<SugenoVariable, Dictionary<ISugenoFunction, double>> functionResults)
        {
            Dictionary<SugenoVariable, double> numerators = new Dictionary<SugenoVariable, double>();
            Dictionary<SugenoVariable, double> denominators = new Dictionary<SugenoVariable, double>();
            Dictionary<SugenoVariable, double> results = new Dictionary<SugenoVariable, double>();

            // Вычислить numerator и denominator отдельно для каждого выхода
            foreach (SugenoVariable var in Output)
            {
                numerators.Add(var, 0.0);
                denominators.Add(var, 0.0);
            }

            foreach (SugenoFuzzyRule rule in ruleWeights.Keys)
            {
                SugenoVariable var = rule.Conclusion.Var;
                double z = functionResults[var][rule.Conclusion.Term];
                double w = ruleWeights[rule];

                numerators[var] += z * w;
                denominators[var] += w;
            }

            // Вычислить дроби
            foreach (SugenoVariable var in Output)
            {
                if (denominators[var] == 0.0)
                {
                    results[var] = 0.0;
                }
                else
                {
                    results[var] = numerators[var] / denominators[var];
                }
            }

            return results;
        }

        /// <summary>
        /// Вычислить выход системы нечёткого вывода
        /// </summary>
        /// <param name="inputValues">Входные значения</param>
        /// <returns>Выходные значения</returns>
        public Dictionary<SugenoVariable, double> Calculate(Dictionary<FuzzyVariable, double> inputValues)
        {
            // Нужно как минимум одно правило
            if (_rules.Count == 0)
            {
                throw new Exception("There should be one rule as minimum.");
            }

            // шаг фаззификации
            Dictionary<FuzzyVariable, Dictionary<FuzzyTerm, double>> fuzzifiedInput =
                Fuzzify(inputValues);

            // Вычислить условия
            Dictionary<SugenoFuzzyRule, double> ruleWeights = EvaluateConditions(fuzzifiedInput);

            // Вычисление функций
            Dictionary<SugenoVariable, Dictionary<ISugenoFunction, double>> functionsResult = EvaluateFunctions(inputValues);

            // Объединение вывода
            Dictionary<SugenoVariable, double> result = CombineResult(ruleWeights, functionsResult);

            return result;
        }
    }
}
