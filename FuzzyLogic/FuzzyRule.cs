﻿using System;
using System.Collections.Generic;


namespace FuzzyLogic
{
    // Алиас для заключение для Мамдани
    using FuzzyConclusion = SingleCondition<FuzzyVariable, FuzzyTerm>;

    // Алиас для заключение для Сугено
    using SugenoConclusion = SingleCondition<SugenoVariable, ISugenoFunction>;

    /// <summary>
    /// Условие для нечёткого правила для Мамдани и Сугено
    /// </summary>
    public class FuzzyCondition : SingleCondition<FuzzyVariable, FuzzyTerm>
    {
        HedgeType _hedge = HedgeType.None;

        /// <summary>
        /// Модификатор хеджирования
        /// </summary>
        public HedgeType Hedge
        {
            get { return _hedge; }
            set { _hedge = value; }
        }

        internal FuzzyCondition(FuzzyVariable var, FuzzyTerm term)
            : this(var, term, false)
        {
        }

        internal FuzzyCondition(FuzzyVariable var, FuzzyTerm term, bool not)
            : this(var, term, not, HedgeType.None)
        {
        }

        internal FuzzyCondition(FuzzyVariable var, FuzzyTerm term, bool not, HedgeType hedge)
            : base(var, term, not)
        {
            _hedge = hedge;
        }
    }


    /// <summary>
    /// Операторы 'И', 'ИЛИ'
    /// </summary>
    public enum OperatorType
    {
        /// <summary>
        /// Оператор 'И'
        /// </summary>
        And,
        /// <summary>
        /// Оператор 'ИЛИ'
        /// </summary>
        Or
    }

    /// <summary>
    /// Модификаторы хеджирования
    /// </summary>
    public enum HedgeType
    {
        /// <summary>
        /// Нет хеджирования
        /// </summary>
        None,
        /// <summary>
        /// Кубический корень
        /// </summary>
        Slightly,
        /// <summary>
        /// Квадратный корень
        /// </summary>
        Somewhat,
        /// <summary>
        /// Квадрат
        /// </summary>
        Very,
        /// <summary>
        /// Куб
        /// </summary>
        Extremely
    }

    /// <summary>
    /// Интерфейс для условий в выражении 'if'
    /// </summary>
    public interface ICondition
    {}

    /// <summary>
    /// Единичное условие
    /// </summary>
    public class SingleCondition<VariableType, ValueType> : ICondition
        where VariableType : class, INamedVariable
        where ValueType : class, INamedValue
    {
        VariableType _var = null;
        bool _not = false;
        ValueType _term = null;


        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        internal SingleCondition()
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="var">Лингвистическая переменная, к которой относится условие</param>
        /// <param name="term">Терм в выражении 'переменная is терм'</param>
        internal SingleCondition(VariableType var, ValueType term)
        {
            _var = var;
            _term = term;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="var">Лингвистическая переменная, к которой относится условие</param>
        /// <param name="term">Терм в выражении 'переменная is терм'</param>
        /// <param name="not">Содержит ли условие 'not'</param>
        internal SingleCondition(VariableType var, ValueType term, bool not)
            : this(var, term)
        {
            _not = not;
        }

        /// <summary>
        /// Лингвистическая переменная, к которой относится условие
        /// </summary>
        public VariableType Var
        {
            get { return _var; }
            set { _var = value; }
        }

        /// <summary>
        /// MF инвертирована? 
        /// </summary>
        public bool Not
        {
            get { return _not; }
            set { _not = value; }
        }

        /// <summary>
        /// Терм в выражении 'переменная is терм'
        /// </summary>
        public ValueType Term
        {
            get { return _term; }
            set { _term = value; }
        }
    }

    /// <summary>
    /// Несколько условий, связанные и/или
    /// </summary>
    public class Conditions : ICondition
    {
        bool _not = false;
        OperatorType _op = OperatorType.And;
        List<ICondition> _Conditions = new List<ICondition>();

        /// <summary>
        /// MF инвертирована?
        /// </summary>
        public bool Not
        {
            get { return _not; }
            set { _not = value; }
        }

        /// <summary>
        /// Оператор, который соединяет условия (и/или)
        /// </summary>
        public OperatorType Op
        {
            get { return _op; }
            set { _op = value; }
        }

        /// <summary>
        /// Список условий
        /// </summary>
        public List<ICondition> ConditionsList
        {
            get { return _Conditions; }
        }
    }

    /// <summary>
    /// Интерфейс для парсера условий
    /// </summary>
    interface IParsableRule<OutputVariableType, OutputValueType>
        where OutputVariableType : class, INamedVariable
        where OutputValueType : class, INamedValue
    {
        /// <summary>
        /// Условие правила (IF)
        /// </summary>
        Conditions Condition { get; set; }

        /// <summary>
        /// Заключение правила (THEN)
        /// </summary>
        SingleCondition<OutputVariableType, OutputValueType> Conclusion { get; set; }
    }

    /// <summary>
    /// Реализует общую функциональность для нечётких правил
    /// </summary>
    public abstract class GenericFuzzyRule
    {
        Conditions _condition = new Conditions();

        /// <summary>
        /// Условие правила (IF)
        /// </summary>
        public Conditions Condition
        {
            get { return _condition; }
            set { _condition = value; }
        }

        /// <summary>
        /// Создаёт единичное условие
        /// </summary>
        /// <param name="var">Лингвистическая переменная, к которой относится условие</param>
        /// <param name="term">Терм в выражении вида 'переменная is терм'</param>
        /// <returns>Сгенерированное условие</returns>
        public FuzzyCondition CreateCondition(FuzzyVariable var, FuzzyTerm term)
        {
            return new FuzzyCondition(var, term);
        }

        /// <summary>
        /// Создаёт единичное условие
        /// </summary>
        /// <param name="var">Лингвистическая переменная, к которой относится условие</param>
        /// <param name="term">Терм в выражении вида 'переменная is терм'</param>
        /// <param name="not">Содержит ли условие не ('not')</param>
        /// <returns>Сгенерированное условие</returns>
        public FuzzyCondition CreateCondition(FuzzyVariable var, FuzzyTerm term, bool not)
        {
            return new FuzzyCondition(var, term, not);
        }

        /// <summary>
        /// Создаёт единичное условие
        /// </summary>
        /// <param name="var">Лингвистическая переменная, к которой относится условие</param>
        /// <param name="term">Терм в выражении вида 'переменная is терм'</param>
        /// <param name="not">Содержит ли условие не ('not')</param>
        /// <param name="hedge">Модификатор хеджирования</param>
        /// <returns>Сгенерированное условие</returns>
        public FuzzyCondition CreateCondition(FuzzyVariable var, FuzzyTerm term, bool not, HedgeType hedge)
        {
            return new FuzzyCondition(var, term, not, hedge);
        }
    }

    /// <summary>
    /// Правило для алгоритма Мамдани
    /// </summary>
    public class MamdaniFuzzyRule : GenericFuzzyRule, IParsableRule<FuzzyVariable, FuzzyTerm>
    {
        private FuzzyConclusion _conclusion = new FuzzyConclusion();           
        private double _weight = 1.0;

        /// <summary>
        /// Конструктор. 
        /// Правило нельзя создать напрямую. Для его создания нужно применять методы 
        /// MamdaniFuzzySystem.EmptyRule или MamdaniFuzzySystem.ParseRule
        /// </summary>
        internal MamdaniFuzzyRule()
        {}

        /// <summary>
        /// Заключение правила (THEN)
        /// </summary>
        public FuzzyConclusion Conclusion
        {
            get { return _conclusion; }
            set { _conclusion = value; }
        }

        /// <summary>
        /// Вес правила
        /// </summary>
        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
    }

    /// <summary>
    /// Нечёткое правило для Сугено
    /// </summary>
    public class SugenoFuzzyRule : GenericFuzzyRule, IParsableRule<SugenoVariable, ISugenoFunction>
    {
        SugenoConclusion _conclusion = new SugenoConclusion();

        /// <summary>
        /// Конструктор. Правило нельзя создать напрямую, только используя SugenoFuzzySystem.EmptyRule или SugenoFuzzySystem.ParseRule
        /// </summary>
        internal SugenoFuzzyRule()
        {}

        /// <summary>
        /// Заключение правила (THEN)
        /// </summary>
        public SugenoConclusion Conclusion
        {
            get { return _conclusion; }
            set { _conclusion = value; }
        }
    }
}
