﻿using System;
using System.Collections.Generic;


namespace FuzzyLogic
{
    /// <summary>
    /// Метод вычисления "И" (And)
    /// </summary>
    public enum AndMethod
    {
        /// <summary>
        /// Минимум: min(a, b)
        /// </summary>
        Min,
        /// <summary>
        /// Произведение: a * b
        /// </summary>
        Production
    }


    /// <summary>
    /// Метод вычисления "ИЛИ" (Or)
    /// </summary>
    public enum OrMethod
    {
        /// <summary>
        /// Максимум: max(a, b)
        /// </summary>
        Max,
        /// <summary>
        /// Вероятностная "ИЛИ": a + b - a * b
        /// </summary>
        Probabilistic
    }

    /// <summary>
    /// Нечёткий метод для следования
    /// </summary>
    public enum ImplicationMethod
    {
        /// <summary>
        /// Отсечение выхода нечёткого множества
        /// </summary>
        Min,
        /// <summary>
        /// Масштабирование выхода нечёткого множества
        /// </summary>
        Production
    }

    /// <summary>
    /// Метод аггрегации для функций принадлежности
    /// </summary>
    public enum AggregationMethod
    {
        /// <summary>
        /// Максимум выходов правил
        /// </summary>
        Max,
        /// <summary>
        /// Сумма выхода правила
        /// </summary>
        Sum
    }

    /// <summary>
    /// Метод дефуззификации
    /// </summary>
    public enum DefuzzificationMethod
    {
        /// <summary>
        /// Центр площади нечёткого результата MF
        /// </summary>
        Centroid,
        /// <summary>
        /// Не реализовано
        /// </summary>
        Bisector,
        /// <summary>
        /// Не реализовано
        /// </summary>
        AverageMaximum
    }
}
