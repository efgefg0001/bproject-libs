﻿using System;
using System.Collections.Generic;


namespace FuzzyLogic
{
    /// <summary>
    /// Интерфейс, который должен быть реализован классом, чтобы использоваться как 
    /// выходная функция в Сугено
    /// </summary>
    public interface ISugenoFunction : INamedValue
    {
        /// <summary>
        /// Вычислить результат функции 
        /// </summary>
        /// <param name="inputValues">Входные значения</param>
        /// <returns>Результат вычисления</returns>
        double Evaluate(Dictionary<FuzzyVariable, double> inputValues);
    }

    /// <summary>
    /// Линейная функция для Сугено 
    /// (можно создавать через SugenoFuzzySystem.CreateSugenoFunction метод)
    /// </summary>
    public class LinearSugenoFunction : NamedValueImpl, ISugenoFunction
    {
        List<FuzzyVariable> _input = null;
        Dictionary<FuzzyVariable, double> _coeffs = new Dictionary<FuzzyVariable,double>();
        double _constValue = 0.0;

        /// <summary>
        /// Получить или установить коэффициент
        /// </summary>
        public double ConstValue
        {
            get { return _constValue; }
            set { _constValue = value; }
        }

        /// <summary>
        /// Получить коэффициент по нечёткой переменной
        /// </summary>
        /// <param name="var">Нечёткая переменная</param>
        /// <returns>Значение коэффициента</returns>
        public double GetCoefficient(FuzzyVariable var)
        {
            if (var == null)
            {
                return _constValue;
            }
            else
            {
                return _coeffs[var];
            }
        }

        /// <summary>
        /// Установить коэффициент по нечёткой переменной
        /// </summary>
        /// <param name="var">Нечёткая переменная</param>
        /// <param name="coeff">Новое значение коэффициента</param>
        public void SetCoefficient(FuzzyVariable var, double coeff)
        {
            if (var == null)
            {
                _constValue = coeff;
            }
            else
            {
                _coeffs[var] = coeff;
            }
        }

        internal LinearSugenoFunction(string name, List<FuzzyVariable> input) : base(name)
        {
            _input = input;
        }

        internal LinearSugenoFunction(string name, List<FuzzyVariable> input, Dictionary<FuzzyVariable, double> coeffs, double constValue)
            : this (name, input)
        {
            // Проверить, что все коэффициенты связаны с переменной
            foreach (FuzzyVariable var in coeffs.Keys)
            {
                if (!_input.Contains(var))
                {
                    throw new ArgumentException(string.Format(
                        "Input of the fuzzy system does not contain '{0}' variable.",
                        var.Name));
                }
            }

            // Инициализировать члены
            _coeffs = coeffs;
            _constValue = constValue;
        }

        internal LinearSugenoFunction(string name, List<FuzzyVariable> input, double[] coeffs)
            : this(name, input)
        {
            // Проверить входные значения
            if (coeffs.Length != input.Count && coeffs.Length != input.Count + 1)
            {
                throw new ArgumentException("Wrong lenght of coefficients' array");
            }
            
            // Заполнить список коэффициентов
            for (int i = 0; i < input.Count; i++)
            {
                _coeffs.Add(input[i], coeffs[i]);
            }

            if (coeffs.Length == input.Count + 1)
            {
                _constValue = coeffs[coeffs.Length - 1];
            }
        }

        /// <summary>
        /// Вычислить результат линейной функции
        /// </summary>
        /// <param name="inputValues">Входные значения</param>
        /// <returns>Результат вычисления</returns>
        public double Evaluate(Dictionary<FuzzyVariable, double> inputValues)
        {
            // входные значения следует проверять
            double result = 0.0;

            foreach (FuzzyVariable var in _coeffs.Keys)
            {
                result += _coeffs[var] * inputValues[var];
            }
            result += _constValue;

            return result;
        }
    }

    /// <summary>
    /// Использовать как выходную переменную в Сугено
    /// </summary>
    public class SugenoVariable : NamedVariableImpl
    {
        List<ISugenoFunction> _functions = new List<ISugenoFunction>();

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя переменной</param>
        public SugenoVariable(string name) : base(name)
        {
        }

        /// <summary>
        /// Список функций, которые пренадлежат переменной
        /// </summary>
        public List<ISugenoFunction> Functions
        {
            get
            {
                return _functions;
            }
        }

        /// <summary>
        /// Список функций, которые принадлежат переменной (реализация INamedVariable)
        /// </summary>
        public override List<INamedValue> Values
        {
            get
            {
                List<INamedValue> values = new List<INamedValue>();
                foreach (ISugenoFunction val in _functions)
                {
                    values.Add(val);
                }
                return values;
            }
        }

        /// <summary>
        /// Найти функцию по её имени
        /// </summary>
        /// <param name="name">Имя функции</param>
        /// <returns>Найденная функция</returns>
        public ISugenoFunction GetFuncByName(string name)
        {
            foreach (NamedValueImpl func in Values)
            {
                if (func.Name == name)
                {
                    return (ISugenoFunction)func;
                }
            }

            throw new KeyNotFoundException();
        }
    }
}
