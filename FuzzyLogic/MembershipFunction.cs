﻿using System;
using System.Collections.Generic;


namespace FuzzyLogic
{
    /// <summary>
    /// Типы композиции функций принадлежности
    /// </summary>
    public enum MfCompositionType
    {
        /// <summary>
        /// Минимум функций
        /// </summary>
        Min,
        /// <summary>
        /// Максимум функций
        /// </summary>
        Max,
        /// <summary>
        /// Произведение функций
        /// </summary>
        Prod,
        /// <summary>
        /// Сумма функций
        /// </summary>
        Sum
    }

    /// <summary>
    /// Абстрактный базовый класс для функций принадлежности
    /// </summary>
    public abstract class MembershipFunction
    {
        /// <param name="x">Аргумент x</param>
        /// <returns>Значение функции принадлежности</returns>
        public abstract double GetValue(double x);

        /// <summary>
        /// Точность
        /// </summary>
        public readonly double EPS = 1e-5;

    }

    /// <summary>
    /// Треугольная функция принадлежности
    /// </summary>
    public class TriangularMembershipFunction : MembershipFunction
    {
        private double _x1, _x2, _x3;

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public TriangularMembershipFunction()
        {}

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="x1">Абсцисса точки 1</param>
        /// <param name="x2">Абсцисса точки 2</param>
        /// <param name="x3">Абсцисса точки 3</param>
        public TriangularMembershipFunction(double x1, double x2, double x3)
        {
            if (!(x1 <= x2 && x2 <= x3))
            {
                throw new ArgumentException();
            }

            _x1 = x1;
            _x2 = x2;
            _x3 = x3;
        }

        /// <summary>
        /// Абсцисса точки 1
        /// </summary>
        public double X1
        {
            get { return _x1; }
            set { _x1 = value; }
        }

        /// <summary>
        /// Абсцисса точки 2
        /// </summary>
        public double X2
        {
            get { return _x2; }
            set { _x2 = value; }
        }

        /// <summary>
        /// Абсцисса точки 3
        /// </summary>
        public double X3
        {
            get { return _x3; }
            set { _x3 = value; }
        }

        /// <param name="x">Аргумент x</param>
        /// <returns>Значение функции принадлежности</returns>        
        public override double GetValue(double x)
        {
            var result = 0.0;
            if (Math.Abs(x - _x1) < EPS && Math.Abs(x - _x2) < EPS)
            {
                result = 1.0;
            }
            else if (Math.Abs(x - _x2) < EPS && Math.Abs(x - _x3) < EPS)
            {
                result = 1.0;
            }
            else if (x <= _x1 || x >= _x3)
            {
                result = 0;
            }
            else if (x == _x2)
            {
                result = 1;
            }
            else if ((x > _x1) && (x < _x2))
            {

                result = (x - _x1)/ (_x2 - _x1);
            }
            else
            {
                result = (_x3 - x) / (_x3 - _x2);
            }
            return result;
        }

        /// <summary>
        /// Приблизительно преобразует в нормальную функцию принадлежности
        /// </summary>
        /// <returns></returns>
        public NormalMembershipFunction ToNormalMF()
        {
            double b = _x2;
            double sigma25 = (_x3 - _x1) / 2.0;
            double sigma = sigma25 / 2.5;
            return new NormalMembershipFunction(b, sigma);
        }
    }


    /// <summary>
    /// Трапецевидная функция принадлежности
    /// </summary>
    public class TrapezoidMembershipFunction : MembershipFunction
    {
        private double _x1, _x2, _x3, _x4;


        /// <summary>
        /// Конструктор
        /// </summary>
        public TrapezoidMembershipFunction()
        {}


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="x1">Абсцисса 1</param>
        /// <param name="x2">Абсцисса 2</param>
        /// <param name="x3">Абсцисса 3</param>
        /// <param name="x4">Абсцисса 4</param>
        public TrapezoidMembershipFunction(double x1, double x2, double x3, double x4)
        {
            if (!(x1 <= x2 && x2 <= x3 && x3 <= x4))
            {
                throw new ArgumentException();
            }

            _x1 = x1;
            _x2 = x2;
            _x3 = x3;
            _x4 = x4;
        }

        /// <summary>
        /// Абсцисса 1
        /// </summary>
        public double X1
        {
            get { return _x1; }
            set { _x1 = value; }
        }

        /// <summary>
        /// Абсцисса 2
        /// </summary>
        public double X2
        {
            get { return _x2; }
            set { _x2 = value; }
        }

        /// <summary>
        /// Абсцисса 3
        /// </summary>
        public double X3
        {
            get { return _x3; }
            set { _x3 = value; }
        }

        /// <summary>
        /// Абсцисса 4
        /// </summary>
        public double X4
        {
            get { return _x4; }
            set { _x4 = value; }
        }

        /// <summary>
        /// Вычислить значение функции принадлежности
        /// </summary>
        /// <param name="x">Аргумент (значение x)</param>
        /// <returns></returns>
        public override double GetValue(double x)
        {
            double result = 0;

            if (x == _x1 && x == _x2)
            {
                result = 1.0;
            }
            else if (x == _x3 && x == _x4)
            {
                result = 1.0;
            }
            else if (x <= _x1 || x >= _x4)
            {
                result = 0;
            }
            else if ((x >= _x2) && (x <= _x3))
            {
                result = 1;
            }
            else if ((x > _x1) && (x < _x2))
            {
                result = (x / (_x2 - _x1)) - (_x1 / (_x2 - _x1));
            }
            else
            {
                result = (-x / (_x4 - _x3)) + (_x4 / (_x4 - _x3));
            }

            return result;
        }
    }

    /// <summary>
    /// Нормальная функция принадлежности
    /// </summary>
    public class NormalMembershipFunction : MembershipFunction
    {
        private double _b = 0.0, _sigma = 1.0;

        /// <summary>
        /// Конструктор
        /// </summary>
        public NormalMembershipFunction()
        {}

        /// <summary>
        /// Конструктор 
        /// </summary>
        /// <param name="b">Параметр b (центр MF)</param>
        /// <param name="sigma">сигма</param>
        public NormalMembershipFunction(double b, double sigma)
        {
            _b = b;
            _sigma = sigma;
        }

        /// <summary>
        /// Параметр b (центр MF)
        /// </summary>
        public double B
        {
            get { return _b; }
            set { _b = value; }
        }

        /// <summary>
        /// Сигма
        /// </summary>
        public double Sigma
        {
            get { return _sigma; }
            set { _sigma = value; }
        }

        /// <summary>
        /// Вычислить значение функции принадлежности
        /// </summary>
        /// <param name="x">Аргумент (значение x)</param>
        /// <returns></returns>
        public override double GetValue(double x)
        {
            return Math.Exp(-(x - _b) * (x - _b) / (2.0 * _sigma * _sigma));
        }
    }

    /// <summary>
    /// Константная функция принадлежности
    /// </summary>
    public class ConstantMembershipFunction : MembershipFunction
    {
        private double _constValue;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="constValue">Постоянное значение</param>
        public ConstantMembershipFunction(double constValue)
        {
            if (constValue < 0.0 || constValue > 1.0)
            {
                throw new ArgumentException();
            }

            _constValue = constValue;
        }

        /// <summary>
        /// Вычислить значение
        /// </summary>
        /// <param name="x">Аргумент x</param>
        /// <returns></returns>
        public override double GetValue(double x)
        {
            return _constValue;
        }
    }


    /// <summary>
    /// Композиция нескольких функций принадлежности, представленных как одна функция принадлежности
    /// </summary>
    internal class CompositeMembershipFunction : MembershipFunction
    {
        private List<MembershipFunction> _mfs = new List<MembershipFunction>();
        private MfCompositionType _composType;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="composType">Тип функции композиции</param>
        public CompositeMembershipFunction(MfCompositionType composType)
        {
            _composType = composType;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="composType">Тип композиции функций принадлежности</param>
        /// <param name="mf1">Функция принадлежности 1</param>
        /// <param name="mf2">Функция принадлежности 2</param>
        public CompositeMembershipFunction(
            MfCompositionType composType,
            MembershipFunction mf1,
            MembershipFunction mf2) : this(composType)
        {
            _mfs.Add(mf1);
            _mfs.Add(mf2);
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="composType">Тип композиции</param>
        /// <param name="mfs">Функции принадлежности</param>
        public CompositeMembershipFunction(
                MfCompositionType composType,
                List<MembershipFunction> mfs)
            : this(composType)
        {
            _mfs = mfs;
        }

        /// <summary>
        /// Список функций принадлежности
        /// </summary>
        public List<MembershipFunction> MembershipFunctions
        {
            get { return _mfs; }
        }

        /// <summary>
        /// Тип композиции функций принадлежности
        /// </summary>
        public MfCompositionType CompositionType
        {
            get { return _composType; }
            set { _composType = value; }
        }

        /// <summary>
        /// Вычислить значение функции принадлежности
        /// </summary>
        /// <param name="x">Аргумент x</param>
        /// <returns></returns>
        public override double GetValue(double x)
        {
            if (_mfs.Count == 0)
            {
                return 0.0;
            }
            else if (_mfs.Count == 1)
            {
                return _mfs[0].GetValue(x);
            }
            else
            {
                double result = _mfs[0].GetValue(x);
                for (int i = 1; i < _mfs.Count; i++)
                {
                    result = Compose(result, _mfs[i].GetValue(x));
                }
                return result;
            }
        }

        double Compose(double val1, double val2)
        {
            switch (_composType)
            {
                case MfCompositionType.Max:
                    return Math.Max(val1, val2);
                case MfCompositionType.Min:
                    return Math.Min(val1, val2);
                case MfCompositionType.Prod:
                    return val1 * val2;
                case MfCompositionType.Sum:
                    return val1 + val2;
                default:
                    throw new Exception("Internal exception.");
            }
        }
    }
}
