﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using QuadcopterMathModel;

namespace ResearchPart
{
    class Program
    {
        private const double EPS = 0.01; 

        private static QuadcoptersFactory factory = new QuadcoptersFactory();

        private static QuadcopterParams quadcopter;
        private static QuadcopterOdeSolver solverManualPids;
        private static QuadcopterOdeSolver solverMamdaniPids;
        private static QuadcopterOdeSolver solverSugenoPids;

        private static void Init()
        {
            quadcopter = factory.CreateUnstableQuadcopter();
            var time = factory.CreateTime();
            solverManualPids = factory.CreateFilledQuadcopterOdeSolverWithManualPids(quadcopter, time);
            solverMamdaniPids = factory.CreateFilledOdeSolverForMamdaniFuzzyPids(quadcopter, time);
            solverSugenoPids = factory.CreateFilledOdeSolverForSugenoFuzzyPids(quadcopter, time);
        }
        
        private static void Test(Action init)
        {
            init();
            var condition = string.Format(
                "omega1=omega2=omega3=omega4={0}, zDest={1}, zStart={2}, phi={3}, theta={4}, psi={5}",
                quadcopter.Motor1.StartOmega, quadcopter.Zdest, quadcopter.StartPosition.Z, quadcopter.StartAttitude.PhiDegrees, quadcopter.StartAttitude.ThetaDegrees, quadcopter.StartAttitude.PsiDegrees
            );
            Console.WriteLine("==============================");
            Console.WriteLine(condition);
            var resultTestOneManual = solverManualPids.Solve();
            ShowResults(resultTestOneManual, "Manual: ");

            var resultTestOneMamdani = solverMamdaniPids.Solve();
            ShowResults(resultTestOneMamdani, "Mamdani: ");

            var resultTestOneSugeno = solverSugenoPids.Solve();
            ShowResults(resultTestOneMamdani, "Sugeno: ");
            Console.WriteLine("==============================");
        }

        static void Main(string[] args)
        {
            Init();
            
            // test 1
            Test(()=> {
                quadcopter.Zdest = 5; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees=5;
                quadcopter.StartAttitude.ThetaDegrees=8;
                quadcopter.StartAttitude.PsiDegrees=10;
            });

            // test 2
            Test(()=> {
                quadcopter.Zdest = 6; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 3
            Test(()=> {
                quadcopter.Zdest = 6; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 4
            Test(()=> {
                quadcopter.Zdest = 6; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 5
            Test(()=> {
                quadcopter.Zdest = 6; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 6
            Test(()=> {
                quadcopter.Zdest = 6; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 7
            Test(()=> {
                quadcopter.Zdest = 6; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 8
            Test(()=> {
                quadcopter.Zdest = 6; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 9
            Test(()=> {
                quadcopter.Zdest = 6; quadcopter.StartPosition.Z = 1;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 10
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees=5;
                quadcopter.StartAttitude.ThetaDegrees=8;
                quadcopter.StartAttitude.PsiDegrees=10;
            });

            // test 11
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 12
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 13
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 14
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 15
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 16
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 17
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 18
            Test(()=> {
                quadcopter.Zdest = 1; quadcopter.StartPosition.Z = 6;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 19
            Test(()=> {
                quadcopter.Zdest = 0; quadcopter.StartPosition.Z = 0;
                quadcopter.StartAttitude.PhiDegrees = 0;
                quadcopter.StartAttitude.ThetaDegrees = 0;
                quadcopter.StartAttitude.PsiDegrees = 0;
            });

            // test 20
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = 0;
                quadcopter.StartAttitude.ThetaDegrees = 0;
                quadcopter.StartAttitude.PsiDegrees = 0;
            });

            // test 21
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 15;
                quadcopter.StartAttitude.PhiDegrees = 0;
                quadcopter.StartAttitude.ThetaDegrees = 0;
                quadcopter.StartAttitude.PsiDegrees = 0;
            });

            // test 22
            Test(()=> {
                quadcopter.Zdest = 15; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = 0;
                quadcopter.StartAttitude.ThetaDegrees = 0;
                quadcopter.StartAttitude.PsiDegrees = 0;
            });

            // test 23
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 24
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 25
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 26
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 27
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = 45;
            });

            // test 28
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = 45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 29
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = 45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });

            // test 30
            Test(()=> {
                quadcopter.Zdest = 10; quadcopter.StartPosition.Z = 10;
                quadcopter.StartAttitude.PhiDegrees = -45;
                quadcopter.StartAttitude.ThetaDegrees = -45;
                quadcopter.StartAttitude.PsiDegrees = -45;
            });
        }

        private static void ShowResults(QuadcopterOdeResult result, string description)
        {
            Console.Write(description);
            double zStabTime = double.NaN, phiStabTime = double.NaN, thetaStabTime = double.NaN, psiStabTime = double.NaN;
            var t = result.T;
            for (var i = 0; i < t.Count; ++i)
                if (Math.Abs(result.X[i][4] - quadcopter.Zdest) < EPS)
                {
                    zStabTime = t[i];
                    break;
                }
            for (var i = 0; i < t.Count; ++i)
                if (Math.Abs(MathUtils.FromRadiansToDegrees(result.X[i][6]) - quadcopter.DestAttitude.PhiDegrees) < EPS)
                {
                    phiStabTime = t[i];
                    break;
                }
            for (var i = 0; i < t.Count; ++i)
                if (Math.Abs(MathUtils.FromRadiansToDegrees(result.X[i][8]) - quadcopter.DestAttitude.ThetaDegrees) < EPS)
                {
                    thetaStabTime = t[i];
                    break;
                }
            for (var i = 0; i < t.Count; ++i)
                if (MathUtils.FromRadiansToDegrees(Math.Abs(result.X[i][10]) - quadcopter.DestAttitude.PsiDegrees) < EPS)
                {
                    psiStabTime = t[i];
                    break;
                }
            Console.WriteLine("zStabTime={0}, phiStabTime={1}, thetaStabTime={2}, psiStabTime={3}", zStabTime, phiStabTime, thetaStabTime, psiStabTime);
        }
    }
}
