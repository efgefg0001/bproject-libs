﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QuadcopterMathModel;

namespace RungeKutta
{
    public partial class EulerAnglesForm : Form
    {
        private int width = 2;
        private IList<double> t;
        private IList<double[]> x;
        public EulerAnglesForm(IList<double> t, IList<double[]> x)
        {
            InitializeComponent();
            this.t = t;
            this.x = x;
        }

        private void X1SeriesForm_Load(object sender, EventArgs e)
        {
            chart.ChartAreas[0].AxisX.Title = "t, с";
            chart.ChartAreas[0].AxisY.Title = "углы Эйлера, градусы";

            chart.Series.Add("phi");
            chart.Series["phi"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["phi"].BorderWidth = width;
            chart.Series["phi"].LegendText = "крен";

            chart.Series.Add("theta");
            chart.Series["theta"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["theta"].BorderWidth = width;
            chart.Series["theta"].LegendText = "тангаж";

            chart.Series.Add("psi");
            chart.Series["psi"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["psi"].BorderWidth = width;
            chart.Series["psi"].LegendText = "рыскание";

            for (int i = 0, count = Math.Min(t.Count, x.Count); i < count; ++i)
            {
                chart.Series["phi"].Points.AddXY(t[i], MathUtils.FromRadiansToDegrees(x[i][6]));
                chart.Series["theta"].Points.AddXY(t[i], MathUtils.FromRadiansToDegrees(x[i][8]));
                chart.Series["psi"].Points.AddXY(t[i], MathUtils.FromRadiansToDegrees(x[i][10]));
            }
        }
    }
}
