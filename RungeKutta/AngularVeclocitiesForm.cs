﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RungeKutta
{
    public partial class AngularVeclocitiesForm : Form
    {
        private int width = 2;
        private IList<double> t;
        private IList<double[]> omega;

        public AngularVeclocitiesForm(IList<double> t, IList<double []> omega)
        {
            InitializeComponent();

            this.t = t;
            this.omega = omega;
        }

        private void AngularVeclocitiesForm_Load(object sender, EventArgs e)
        {
            chart.ChartAreas[0].AxisX.Title = "t, с";
            chart.ChartAreas[0].AxisY.Title = "угловые скорости винтов, рад/с";

            chart.Series.Add("omega1");
            chart.Series["omega1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["omega1"].BorderWidth = width;
            chart.Series["omega1"].LegendText = "w1";

            chart.Series.Add("omega2");
            chart.Series["omega2"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["omega2"].BorderWidth = width;
            chart.Series["omega2"].LegendText = "w2";

            chart.Series.Add("omega3");
            chart.Series["omega3"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["omega3"].BorderWidth = width;
            chart.Series["omega3"].LegendText = "w3";

            chart.Series.Add("omega4");
            chart.Series["omega4"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["omega4"].BorderWidth = 1;
            chart.Series["omega4"].LegendText = "w4";

            for (int i = 0, count = Math.Min(t.Count, omega.Count); i < count; ++i)
            {
                chart.Series["omega1"].Points.AddXY(t[i], omega[i][0]);
                chart.Series["omega2"].Points.AddXY(t[i], omega[i][1]);
                chart.Series["omega3"].Points.AddXY(t[i], omega[i][2]);
                chart.Series["omega4"].Points.AddXY(t[i], omega[i][3]);
            }
        }
    }
}
