﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QuadcopterMathModel;

namespace RungeKutta
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private Screen screen;
        private QuadcoptersFactory factory;
        private QuadcopterOdeSolver solver;
        private void Form1_Load(object sender, EventArgs e)
        {
            screen = Screen.FromControl(this);
            this.CenterToScreen();

            factory = new QuadcoptersFactory();
            pidsComboBox.Items.Add(factory.CreateFilledQuadcopterOdeSolverWithManualPids());
            pidsComboBox.Items.Add(factory.CreateFilledOdeSolverForMamdaniFuzzyPids());
            pidsComboBox.Items.Add(factory.CreateFilledOdeSolverForSugenoFuzzyPids());
            pidsComboBox.SelectedIndex = 0;
            solver = (QuadcopterOdeSolver)pidsComboBox.SelectedItem;
            ShowQuadcopterValues();
        }
        private void ShowQuadcopterValues()
        {
            var quadcopter = solver.Quadcopter;
            omega1TextBox.Text = quadcopter.Omega1.ToString();
            omega2TextBox.Text = quadcopter.Omega2.ToString();
            omega3TextBox.Text = quadcopter.Omega3.ToString();
            omega4TextBox.Text = quadcopter.Omega4.ToString();

            xTextBox.Text = quadcopter.InitialValsForFuncs[0].ToString();
            yTextBox.Text = quadcopter.InitialValsForFuncs[2].ToString();
            zTextBox.Text = quadcopter.InitialValsForFuncs[4].ToString();

            phiTextBox.Text = MathUtils.FromRadiansToDegrees(quadcopter.InitialValsForFuncs[6]).ToString();
            thetaTextBox.Text = MathUtils.FromRadiansToDegrees(quadcopter.InitialValsForFuncs[8]).ToString();
            psiTextBox.Text = MathUtils.FromRadiansToDegrees(quadcopter.InitialValsForFuncs[10]).ToString();

            destZTextBox.Text = quadcopter.Zdest.ToString();
            destPhiTextBox.Text = MathUtils.FromRadiansToDegrees(quadcopter.PhiDest).ToString();
            destThetaTextBox.Text = MathUtils.FromRadiansToDegrees(quadcopter.ThetaDest).ToString();
            destPsiTextBox.Text = MathUtils.FromRadiansToDegrees(quadcopter.PsiDest).ToString();

            tMaxTextBox.Text = solver.Time.TMax.ToString();
            dtTextBox.Text = solver.Time.H.ToString();
        }

        private void ShowResults(QuadcopterOdeResult result)
        {
            var positionsForm = new PositionsForm(result.T, result.X);
            positionsForm.StartPosition = FormStartPosition.Manual;
            positionsForm.Location = new Point(0, 0);
            positionsForm.Show();

            var eulerAnglesForm = new EulerAnglesForm(result.T, result.X);
            eulerAnglesForm.StartPosition = FormStartPosition.Manual;
            eulerAnglesForm.Location = new Point(screen.WorkingArea.Width - eulerAnglesForm.Width, 0);
            eulerAnglesForm.Show();

            var angularVelocities = new AngularVeclocitiesForm(result.T, result.Omega);
            angularVelocities.StartPosition = FormStartPosition.Manual;
            angularVelocities.Location = new Point(0, screen.WorkingArea.Height - angularVelocities.Height);
            angularVelocities.Show();

            var velocities = new VelocitiesForm(result.T, result.X);
            velocities.StartPosition = FormStartPosition.Manual;
            velocities.Location = new Point(screen.WorkingArea.Width - velocities.Width, screen.WorkingArea.Height - velocities.Height);
            velocities.Show();
        }

        private double omega1, omega2, omega3, omega4;
        private double x, y, z;
        private double phi, theta, psi;

        private void pidsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            solver = (QuadcopterOdeSolver)pidsComboBox.SelectedItem;
            ShowQuadcopterValues();
        }

        private double phiDest, thetaDest, psiDest;
        private double zDest;
        private double tMax, dt;
        
        private void ReadInputValues()
        {
            solver = (QuadcopterOdeSolver)pidsComboBox.SelectedItem;

            omega1 = double.Parse(omega1TextBox.Text);
            omega2 = double.Parse(omega2TextBox.Text);
            omega3 = double.Parse(omega3TextBox.Text);
            omega4 = double.Parse(omega4TextBox.Text);

            x = double.Parse(xTextBox.Text);
            y = double.Parse(yTextBox.Text);
            z = double.Parse(zTextBox.Text);

            phi = MathUtils.FromDegreesToRadians(double.Parse(phiTextBox.Text));
            theta = MathUtils.FromDegreesToRadians(double.Parse(thetaTextBox.Text));
            psi = MathUtils.FromDegreesToRadians(double.Parse(psiTextBox.Text));

            phiDest = MathUtils.FromDegreesToRadians(double.Parse(destPhiTextBox.Text));
            thetaDest = MathUtils.FromDegreesToRadians(double.Parse(destThetaTextBox.Text));
            psiDest = MathUtils.FromDegreesToRadians(double.Parse(destPsiTextBox.Text));
            zDest = double.Parse(destZTextBox.Text);

            tMax = double.Parse(tMaxTextBox.Text);
            dt = double.Parse(dtTextBox.Text);

            solver.Quadcopter.Omega1 = omega1;
            solver.Quadcopter.Omega2 = omega2;
            solver.Quadcopter.Omega3 = omega3;
            solver.Quadcopter.Omega4 = omega4;

            solver.Quadcopter.InitialValsForFuncs[0] = x;
            solver.Quadcopter.InitialValsForFuncs[2] = y;
            solver.Quadcopter.InitialValsForFuncs[4] = z;

            solver.Quadcopter.InitialValsForFuncs[6] = phi;
            solver.Quadcopter.InitialValsForFuncs[8] = theta;
            solver.Quadcopter.InitialValsForFuncs[10] = psi;

            solver.Quadcopter.Zdest = zDest;
            solver.Quadcopter.PhiDest = phiDest;
            solver.Quadcopter.ThetaDest = thetaDest;
            solver.Quadcopter.PsiDest = psiDest;

            solver.Time.TMax = tMax;
            solver.Time.H = dt;
        }

        private void solveButton_Click(object sender, EventArgs e)
        {
            try {
                ReadInputValues();
                var result = solver.Solve();
                ShowResults(result);
                ReadInputValues();
            } catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }
    }
}
