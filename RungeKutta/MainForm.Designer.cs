﻿namespace RungeKutta
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.solveButton = new System.Windows.Forms.Button();
            this.anglVelGroupBox = new System.Windows.Forms.GroupBox();
            this.omega4TextBox = new System.Windows.Forms.TextBox();
            this.omega4Label = new System.Windows.Forms.Label();
            this.omega3TextBox = new System.Windows.Forms.TextBox();
            this.omega3Label = new System.Windows.Forms.Label();
            this.omega2TextBox = new System.Windows.Forms.TextBox();
            this.omega2Label = new System.Windows.Forms.Label();
            this.omega1TextBox = new System.Windows.Forms.TextBox();
            this.omega1Label = new System.Windows.Forms.Label();
            this.startPosGroupBox = new System.Windows.Forms.GroupBox();
            this.zTextBox = new System.Windows.Forms.TextBox();
            this.zLabel = new System.Windows.Forms.Label();
            this.yTextBox = new System.Windows.Forms.TextBox();
            this.yLabel = new System.Windows.Forms.Label();
            this.xTextBox = new System.Windows.Forms.TextBox();
            this.xLabel = new System.Windows.Forms.Label();
            this.startAngularPosGroupBox = new System.Windows.Forms.GroupBox();
            this.psiTextBox = new System.Windows.Forms.TextBox();
            this.psiLabel = new System.Windows.Forms.Label();
            this.thetaTextBox = new System.Windows.Forms.TextBox();
            this.thetaLabel = new System.Windows.Forms.Label();
            this.phiTextBox = new System.Windows.Forms.TextBox();
            this.phiLabel = new System.Windows.Forms.Label();
            this.destValsGroupBox = new System.Windows.Forms.GroupBox();
            this.destZTextBox = new System.Windows.Forms.TextBox();
            this.destZLabel = new System.Windows.Forms.Label();
            this.destThetaTextBox = new System.Windows.Forms.TextBox();
            this.destThetaLabel = new System.Windows.Forms.Label();
            this.destPsiTextBox = new System.Windows.Forms.TextBox();
            this.destPsiLabel = new System.Windows.Forms.Label();
            this.destPhiTextBox = new System.Windows.Forms.TextBox();
            this.phiDestLabel = new System.Windows.Forms.Label();
            this.tMaxTextBox = new System.Windows.Forms.TextBox();
            this.tMaxLabel = new System.Windows.Forms.Label();
            this.dtTextBox = new System.Windows.Forms.TextBox();
            this.dtLabel = new System.Windows.Forms.Label();
            this.pidsComboBox = new System.Windows.Forms.ComboBox();
            this.anglVelGroupBox.SuspendLayout();
            this.startPosGroupBox.SuspendLayout();
            this.startAngularPosGroupBox.SuspendLayout();
            this.destValsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // solveButton
            // 
            this.solveButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.solveButton.Location = new System.Drawing.Point(206, 507);
            this.solveButton.Name = "solveButton";
            this.solveButton.Size = new System.Drawing.Size(120, 32);
            this.solveButton.TabIndex = 0;
            this.solveButton.Text = "Рассчитать";
            this.solveButton.UseVisualStyleBackColor = true;
            this.solveButton.Click += new System.EventHandler(this.solveButton_Click);
            // 
            // anglVelGroupBox
            // 
            this.anglVelGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.anglVelGroupBox.Controls.Add(this.omega4TextBox);
            this.anglVelGroupBox.Controls.Add(this.omega4Label);
            this.anglVelGroupBox.Controls.Add(this.omega3TextBox);
            this.anglVelGroupBox.Controls.Add(this.omega3Label);
            this.anglVelGroupBox.Controls.Add(this.omega2TextBox);
            this.anglVelGroupBox.Controls.Add(this.omega2Label);
            this.anglVelGroupBox.Controls.Add(this.omega1TextBox);
            this.anglVelGroupBox.Controls.Add(this.omega1Label);
            this.anglVelGroupBox.Location = new System.Drawing.Point(2, 1);
            this.anglVelGroupBox.Name = "anglVelGroupBox";
            this.anglVelGroupBox.Size = new System.Drawing.Size(552, 72);
            this.anglVelGroupBox.TabIndex = 1;
            this.anglVelGroupBox.TabStop = false;
            this.anglVelGroupBox.Text = "Начальные угловые скорости винтов (радианы в секунду)";
            // 
            // omega4TextBox
            // 
            this.omega4TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.omega4TextBox.Location = new System.Drawing.Point(445, 33);
            this.omega4TextBox.Name = "omega4TextBox";
            this.omega4TextBox.Size = new System.Drawing.Size(100, 20);
            this.omega4TextBox.TabIndex = 7;
            // 
            // omega4Label
            // 
            this.omega4Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.omega4Label.AutoSize = true;
            this.omega4Label.Location = new System.Drawing.Point(415, 36);
            this.omega4Label.Name = "omega4Label";
            this.omega4Label.Size = new System.Drawing.Size(33, 13);
            this.omega4Label.TabIndex = 6;
            this.omega4Label.Text = "w4 = ";
            // 
            // omega3TextBox
            // 
            this.omega3TextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.omega3TextBox.Location = new System.Drawing.Point(307, 33);
            this.omega3TextBox.Name = "omega3TextBox";
            this.omega3TextBox.Size = new System.Drawing.Size(100, 20);
            this.omega3TextBox.TabIndex = 5;
            // 
            // omega3Label
            // 
            this.omega3Label.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.omega3Label.AutoSize = true;
            this.omega3Label.Location = new System.Drawing.Point(277, 36);
            this.omega3Label.Name = "omega3Label";
            this.omega3Label.Size = new System.Drawing.Size(33, 13);
            this.omega3Label.TabIndex = 4;
            this.omega3Label.Text = "w3 = ";
            // 
            // omega2TextBox
            // 
            this.omega2TextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.omega2TextBox.Location = new System.Drawing.Point(173, 31);
            this.omega2TextBox.Name = "omega2TextBox";
            this.omega2TextBox.Size = new System.Drawing.Size(100, 20);
            this.omega2TextBox.TabIndex = 3;
            // 
            // omega2Label
            // 
            this.omega2Label.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.omega2Label.AutoSize = true;
            this.omega2Label.Location = new System.Drawing.Point(143, 36);
            this.omega2Label.Name = "omega2Label";
            this.omega2Label.Size = new System.Drawing.Size(33, 13);
            this.omega2Label.TabIndex = 2;
            this.omega2Label.Text = "w2 = ";
            // 
            // omega1TextBox
            // 
            this.omega1TextBox.Location = new System.Drawing.Point(36, 30);
            this.omega1TextBox.Name = "omega1TextBox";
            this.omega1TextBox.Size = new System.Drawing.Size(100, 20);
            this.omega1TextBox.TabIndex = 1;
            // 
            // omega1Label
            // 
            this.omega1Label.AutoSize = true;
            this.omega1Label.Location = new System.Drawing.Point(6, 33);
            this.omega1Label.Name = "omega1Label";
            this.omega1Label.Size = new System.Drawing.Size(33, 13);
            this.omega1Label.TabIndex = 0;
            this.omega1Label.Text = "w1 = ";
            // 
            // startPosGroupBox
            // 
            this.startPosGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.startPosGroupBox.Controls.Add(this.zTextBox);
            this.startPosGroupBox.Controls.Add(this.zLabel);
            this.startPosGroupBox.Controls.Add(this.yTextBox);
            this.startPosGroupBox.Controls.Add(this.yLabel);
            this.startPosGroupBox.Controls.Add(this.xTextBox);
            this.startPosGroupBox.Controls.Add(this.xLabel);
            this.startPosGroupBox.Location = new System.Drawing.Point(2, 79);
            this.startPosGroupBox.Name = "startPosGroupBox";
            this.startPosGroupBox.Size = new System.Drawing.Size(552, 100);
            this.startPosGroupBox.TabIndex = 2;
            this.startPosGroupBox.TabStop = false;
            this.startPosGroupBox.Text = "Начльные координаты квадрокоптера (метры)";
            // 
            // zTextBox
            // 
            this.zTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zTextBox.Location = new System.Drawing.Point(377, 41);
            this.zTextBox.Name = "zTextBox";
            this.zTextBox.Size = new System.Drawing.Size(100, 20);
            this.zTextBox.TabIndex = 13;
            // 
            // zLabel
            // 
            this.zLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zLabel.AutoSize = true;
            this.zLabel.Location = new System.Drawing.Point(347, 44);
            this.zLabel.Name = "zLabel";
            this.zLabel.Size = new System.Drawing.Size(24, 13);
            this.zLabel.TabIndex = 12;
            this.zLabel.Text = "z = ";
            // 
            // yTextBox
            // 
            this.yTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.yTextBox.Location = new System.Drawing.Point(239, 41);
            this.yTextBox.Name = "yTextBox";
            this.yTextBox.Size = new System.Drawing.Size(100, 20);
            this.yTextBox.TabIndex = 11;
            // 
            // yLabel
            // 
            this.yLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.yLabel.AutoSize = true;
            this.yLabel.Location = new System.Drawing.Point(209, 44);
            this.yLabel.Name = "yLabel";
            this.yLabel.Size = new System.Drawing.Size(24, 13);
            this.yLabel.TabIndex = 10;
            this.yLabel.Text = "y = ";
            // 
            // xTextBox
            // 
            this.xTextBox.Location = new System.Drawing.Point(105, 39);
            this.xTextBox.Name = "xTextBox";
            this.xTextBox.Size = new System.Drawing.Size(100, 20);
            this.xTextBox.TabIndex = 9;
            // 
            // xLabel
            // 
            this.xLabel.AutoSize = true;
            this.xLabel.Location = new System.Drawing.Point(75, 44);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(24, 13);
            this.xLabel.TabIndex = 8;
            this.xLabel.Text = "x = ";
            // 
            // startAngularPosGroupBox
            // 
            this.startAngularPosGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.startAngularPosGroupBox.Controls.Add(this.psiTextBox);
            this.startAngularPosGroupBox.Controls.Add(this.psiLabel);
            this.startAngularPosGroupBox.Controls.Add(this.thetaTextBox);
            this.startAngularPosGroupBox.Controls.Add(this.thetaLabel);
            this.startAngularPosGroupBox.Controls.Add(this.phiTextBox);
            this.startAngularPosGroupBox.Controls.Add(this.phiLabel);
            this.startAngularPosGroupBox.Location = new System.Drawing.Point(2, 185);
            this.startAngularPosGroupBox.Name = "startAngularPosGroupBox";
            this.startAngularPosGroupBox.Size = new System.Drawing.Size(552, 100);
            this.startAngularPosGroupBox.TabIndex = 3;
            this.startAngularPosGroupBox.TabStop = false;
            this.startAngularPosGroupBox.Text = "Начальные углы Эйлера кварокоптера (градусы)";
            // 
            // psiTextBox
            // 
            this.psiTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.psiTextBox.Location = new System.Drawing.Point(431, 41);
            this.psiTextBox.Name = "psiTextBox";
            this.psiTextBox.Size = new System.Drawing.Size(100, 20);
            this.psiTextBox.TabIndex = 19;
            // 
            // psiLabel
            // 
            this.psiLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.psiLabel.AutoSize = true;
            this.psiLabel.Location = new System.Drawing.Point(356, 44);
            this.psiLabel.Name = "psiLabel";
            this.psiLabel.Size = new System.Drawing.Size(69, 13);
            this.psiLabel.TabIndex = 18;
            this.psiLabel.Text = "рыскание = ";
            // 
            // thetaTextBox
            // 
            this.thetaTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.thetaTextBox.Location = new System.Drawing.Point(239, 41);
            this.thetaTextBox.Name = "thetaTextBox";
            this.thetaTextBox.Size = new System.Drawing.Size(100, 20);
            this.thetaTextBox.TabIndex = 17;
            // 
            // thetaLabel
            // 
            this.thetaLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.thetaLabel.AutoSize = true;
            this.thetaLabel.Location = new System.Drawing.Point(182, 44);
            this.thetaLabel.Name = "thetaLabel";
            this.thetaLabel.Size = new System.Drawing.Size(55, 13);
            this.thetaLabel.TabIndex = 16;
            this.thetaLabel.Text = "тангаж = ";
            // 
            // phiTextBox
            // 
            this.phiTextBox.Location = new System.Drawing.Point(76, 41);
            this.phiTextBox.Name = "phiTextBox";
            this.phiTextBox.Size = new System.Drawing.Size(100, 20);
            this.phiTextBox.TabIndex = 15;
            // 
            // phiLabel
            // 
            this.phiLabel.AutoSize = true;
            this.phiLabel.Location = new System.Drawing.Point(33, 44);
            this.phiLabel.Name = "phiLabel";
            this.phiLabel.Size = new System.Drawing.Size(43, 13);
            this.phiLabel.TabIndex = 14;
            this.phiLabel.Text = "крен = ";
            // 
            // destValsGroupBox
            // 
            this.destValsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destValsGroupBox.Controls.Add(this.destZTextBox);
            this.destValsGroupBox.Controls.Add(this.destZLabel);
            this.destValsGroupBox.Controls.Add(this.destThetaTextBox);
            this.destValsGroupBox.Controls.Add(this.destThetaLabel);
            this.destValsGroupBox.Controls.Add(this.destPsiTextBox);
            this.destValsGroupBox.Controls.Add(this.destPsiLabel);
            this.destValsGroupBox.Controls.Add(this.destPhiTextBox);
            this.destValsGroupBox.Controls.Add(this.phiDestLabel);
            this.destValsGroupBox.Location = new System.Drawing.Point(2, 291);
            this.destValsGroupBox.Name = "destValsGroupBox";
            this.destValsGroupBox.Size = new System.Drawing.Size(552, 100);
            this.destValsGroupBox.TabIndex = 4;
            this.destValsGroupBox.TabStop = false;
            this.destValsGroupBox.Text = "Целевые значения (градусы и метры)";
            // 
            // destZTextBox
            // 
            this.destZTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.destZTextBox.Location = new System.Drawing.Point(307, 68);
            this.destZTextBox.Name = "destZTextBox";
            this.destZTextBox.Size = new System.Drawing.Size(100, 20);
            this.destZTextBox.TabIndex = 15;
            // 
            // destZLabel
            // 
            this.destZLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.destZLabel.AutoSize = true;
            this.destZLabel.Location = new System.Drawing.Point(277, 71);
            this.destZLabel.Name = "destZLabel";
            this.destZLabel.Size = new System.Drawing.Size(24, 13);
            this.destZLabel.TabIndex = 14;
            this.destZLabel.Text = "z = ";
            // 
            // destThetaTextBox
            // 
            this.destThetaTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.destThetaTextBox.Location = new System.Drawing.Point(308, 42);
            this.destThetaTextBox.Name = "destThetaTextBox";
            this.destThetaTextBox.Size = new System.Drawing.Size(100, 20);
            this.destThetaTextBox.TabIndex = 13;
            // 
            // destThetaLabel
            // 
            this.destThetaLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.destThetaLabel.AutoSize = true;
            this.destThetaLabel.Location = new System.Drawing.Point(255, 48);
            this.destThetaLabel.Name = "destThetaLabel";
            this.destThetaLabel.Size = new System.Drawing.Size(55, 13);
            this.destThetaLabel.TabIndex = 12;
            this.destThetaLabel.Text = "тангаж = ";
            // 
            // destPsiTextBox
            // 
            this.destPsiTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.destPsiTextBox.Location = new System.Drawing.Point(146, 71);
            this.destPsiTextBox.Name = "destPsiTextBox";
            this.destPsiTextBox.Size = new System.Drawing.Size(100, 20);
            this.destPsiTextBox.TabIndex = 11;
            // 
            // destPsiLabel
            // 
            this.destPsiLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.destPsiLabel.AutoSize = true;
            this.destPsiLabel.Location = new System.Drawing.Point(75, 75);
            this.destPsiLabel.Name = "destPsiLabel";
            this.destPsiLabel.Size = new System.Drawing.Size(69, 13);
            this.destPsiLabel.TabIndex = 10;
            this.destPsiLabel.Text = "рыскание = ";
            // 
            // destPhiTextBox
            // 
            this.destPhiTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.destPhiTextBox.Location = new System.Drawing.Point(146, 45);
            this.destPhiTextBox.Name = "destPhiTextBox";
            this.destPhiTextBox.Size = new System.Drawing.Size(100, 20);
            this.destPhiTextBox.TabIndex = 9;
            // 
            // phiDestLabel
            // 
            this.phiDestLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.phiDestLabel.AutoSize = true;
            this.phiDestLabel.Location = new System.Drawing.Point(97, 49);
            this.phiDestLabel.Name = "phiDestLabel";
            this.phiDestLabel.Size = new System.Drawing.Size(43, 13);
            this.phiDestLabel.TabIndex = 8;
            this.phiDestLabel.Text = "крен = ";
            // 
            // tMaxTextBox
            // 
            this.tMaxTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tMaxTextBox.Location = new System.Drawing.Point(144, 410);
            this.tMaxTextBox.Name = "tMaxTextBox";
            this.tMaxTextBox.Size = new System.Drawing.Size(100, 20);
            this.tMaxTextBox.TabIndex = 6;
            // 
            // tMaxLabel
            // 
            this.tMaxLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tMaxLabel.AutoSize = true;
            this.tMaxLabel.Location = new System.Drawing.Point(124, 413);
            this.tMaxLabel.Name = "tMaxLabel";
            this.tMaxLabel.Size = new System.Drawing.Size(22, 13);
            this.tMaxLabel.TabIndex = 5;
            this.tMaxLabel.Text = "t = ";
            // 
            // dtTextBox
            // 
            this.dtTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtTextBox.Location = new System.Drawing.Point(282, 411);
            this.dtTextBox.Name = "dtTextBox";
            this.dtTextBox.Size = new System.Drawing.Size(100, 20);
            this.dtTextBox.TabIndex = 8;
            // 
            // dtLabel
            // 
            this.dtLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtLabel.AutoSize = true;
            this.dtLabel.Location = new System.Drawing.Point(257, 414);
            this.dtLabel.Name = "dtLabel";
            this.dtLabel.Size = new System.Drawing.Size(28, 13);
            this.dtLabel.TabIndex = 7;
            this.dtLabel.Text = "dt = ";
            // 
            // pidsComboBox
            // 
            this.pidsComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pidsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pidsComboBox.Location = new System.Drawing.Point(95, 449);
            this.pidsComboBox.Name = "pidsComboBox";
            this.pidsComboBox.Size = new System.Drawing.Size(332, 21);
            this.pidsComboBox.TabIndex = 9;
            this.pidsComboBox.SelectedIndexChanged += new System.EventHandler(this.pidsComboBox_SelectedIndexChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 542);
            this.Controls.Add(this.pidsComboBox);
            this.Controls.Add(this.dtTextBox);
            this.Controls.Add(this.dtLabel);
            this.Controls.Add(this.tMaxTextBox);
            this.Controls.Add(this.tMaxLabel);
            this.Controls.Add(this.destValsGroupBox);
            this.Controls.Add(this.startAngularPosGroupBox);
            this.Controls.Add(this.startPosGroupBox);
            this.Controls.Add(this.anglVelGroupBox);
            this.Controls.Add(this.solveButton);
            this.Name = "MainForm";
            this.Text = "Задание параметров";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.anglVelGroupBox.ResumeLayout(false);
            this.anglVelGroupBox.PerformLayout();
            this.startPosGroupBox.ResumeLayout(false);
            this.startPosGroupBox.PerformLayout();
            this.startAngularPosGroupBox.ResumeLayout(false);
            this.startAngularPosGroupBox.PerformLayout();
            this.destValsGroupBox.ResumeLayout(false);
            this.destValsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button solveButton;
        private System.Windows.Forms.GroupBox anglVelGroupBox;
        private System.Windows.Forms.GroupBox startPosGroupBox;
        private System.Windows.Forms.GroupBox startAngularPosGroupBox;
        private System.Windows.Forms.GroupBox destValsGroupBox;
        private System.Windows.Forms.TextBox omega1TextBox;
        private System.Windows.Forms.Label omega1Label;
        private System.Windows.Forms.TextBox omega4TextBox;
        private System.Windows.Forms.Label omega4Label;
        private System.Windows.Forms.TextBox omega3TextBox;
        private System.Windows.Forms.Label omega3Label;
        private System.Windows.Forms.TextBox omega2TextBox;
        private System.Windows.Forms.Label omega2Label;
        private System.Windows.Forms.TextBox zTextBox;
        private System.Windows.Forms.Label zLabel;
        private System.Windows.Forms.TextBox yTextBox;
        private System.Windows.Forms.Label yLabel;
        private System.Windows.Forms.TextBox xTextBox;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.Label psiLabel;
        private System.Windows.Forms.TextBox thetaTextBox;
        private System.Windows.Forms.Label thetaLabel;
        private System.Windows.Forms.TextBox phiTextBox;
        private System.Windows.Forms.Label phiLabel;
        private System.Windows.Forms.TextBox psiTextBox;
        private System.Windows.Forms.Label phiDestLabel;
        private System.Windows.Forms.TextBox destPhiTextBox;
        private System.Windows.Forms.Label destPsiLabel;
        private System.Windows.Forms.TextBox destPsiTextBox;
        private System.Windows.Forms.Label destThetaLabel;
        private System.Windows.Forms.TextBox destThetaTextBox;
        private System.Windows.Forms.Label destZLabel;
        private System.Windows.Forms.TextBox destZTextBox;
        private System.Windows.Forms.TextBox tMaxTextBox;
        private System.Windows.Forms.Label tMaxLabel;
        private System.Windows.Forms.Label dtLabel;
        private System.Windows.Forms.TextBox dtTextBox;
        private System.Windows.Forms.ComboBox pidsComboBox;
    }
}

