﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RungeKutta
{
    public partial class VelocitiesForm : Form
    {
        private int width = 2;
        private IList<double> t;
        private IList<double[]> x;
        public VelocitiesForm(IList<double> t, IList<double[]> x)
        {
            InitializeComponent();
            this.t = t;
            this.x = x;
        }

        private void VelocitiesForm_Load(object sender, EventArgs e)
        {
            chart.ChartAreas[0].AxisX.Title = "t, с";
            chart.ChartAreas[0].AxisY.Title = "скорости поступ. движ-я, м/с";

            chart.Series.Add("Vx");
            chart.Series["Vx"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["Vx"].BorderWidth = width;
            chart.Series["Vx"].LegendText = "Vx";

            chart.Series.Add("Vy");
            chart.Series["Vy"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["Vy"].BorderWidth = width;
            chart.Series["Vy"].LegendText = "Vy";

            chart.Series.Add("Vz");
            chart.Series["Vz"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["Vz"].BorderWidth = width;
            chart.Series["Vz"].LegendText = "Vz";

            for (int i = 0, count = Math.Min(t.Count, x.Count); i < count; ++i)
            {
                chart.Series["Vx"].Points.AddXY(t[i], x[i][1]);
                chart.Series["Vy"].Points.AddXY(t[i], x[i][3]);
                chart.Series["Vz"].Points.AddXY(t[i], x[i][5]);
            }
        }
    }
}
