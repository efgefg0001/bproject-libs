﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RungeKutta
{
    public partial class PositionsForm : Form
    {
        private int width = 2;
        private IList<double> t;
        private IList<double[]> x;
        public PositionsForm(IList<double> t, IList<double[]> x)
        {
            InitializeComponent();
            this.t = t;
            this.x= x;
        }

        private void X0SeriesForm_Load(object sender, EventArgs e)
        {
            chart.ChartAreas[0].AxisX.Title = "t, с";
            chart.ChartAreas[0].AxisY.Title = "координаты, м";

            chart.Series.Add("x");
            chart.Series["x"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["x"].BorderWidth = width;
            chart.Series["x"].LegendText = "x";

            chart.Series.Add("y");
            chart.Series["y"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["y"].BorderWidth = width;
            chart.Series["y"].LegendText = "y";

            chart.Series.Add("z");
            chart.Series["z"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart.Series["z"].BorderWidth = width;
            chart.Series["z"].LegendText = "z";

            for (int i = 0, count = Math.Min(t.Count, x.Count); i < count; ++i)
            {
                chart.Series["x"].Points.AddXY(t[i], x[i][0]);
                chart.Series["y"].Points.AddXY(t[i], x[i][2]);
                chart.Series["z"].Points.AddXY(t[i], x[i][4]);
            }
        }
    }
}
