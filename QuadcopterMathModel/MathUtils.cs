﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FuzzyLogic;

namespace QuadcopterMathModel
{
    public static class MathUtils
    {
        public const double g = 9.81;
        public static double TrapezoidIntegration(Func<double, double> f, double min, double max, double n = 10)
        {
            var step = (max - min) / n;
            var integral = (f(min) + f(max)) * 0.5;
            int i = 1;
            for (var t = min + step; i <= n - 1; t += step, ++i)
                integral += f(t);
            integral = step * integral;
            return integral;
        }

        public static double FromRadiansToDegrees(double radians)
        {
            double degrees = radians * 180 / Math.PI;
            return degrees;
        }

        public static double FromDegreesToRadians(double degrees)
        {
            double radians = Math.PI * degrees / 180;
            return radians;
        }

        public static double ConvertMinMax(double x2, double min2, double max2, double min1, double max1)
        {
            var res = 0.0;
            if (max2 != min2)
                res = min1 + (x2 - min2) / (max2 - min2) * (max1 - min1);
            return res;

        }

        public static double NormalizeZeroOne(double value, double min, double max)
        {
            var result = ConvertMinMax(value, min, max, 0, 1);
            return result;
        }

        public static double Normalize(double value, double min, double max)
        {
            /*            var result = 0.0;
                        if (min != max)
                            result = (value - min) / (max - min);*/
            var result = MathUtils.ConvertMinMax(value, min, max, -1, 1);
            return result;
        }
        
        public static double RestoreFromNormalizedZeroOne(double norm, double min, double max)
        {
            var result = ConvertMinMax(norm, 0, 1, min, max);
            return result;
        }

        public static double RestoreFromNormalized(double norm, double min, double max)
        {
            /*            var result = min + (max - min) * norm;
                        return result;*/
            var result = MathUtils.ConvertMinMax(norm, -1, 1, min, max);
            return result;
        }

        public static double ComputeCoeff(MamdaniFuzzySystem fuzzySystem, double nRateOfE, double nError, string nameOfCoeff, double kpMin, double kpMax)
        {
            try {
                var error = fuzzySystem.InputByName("error");
                var rateOfError = fuzzySystem.InputByName("rateOfError");
                var param = fuzzySystem.OutputByName(nameOfCoeff);

                var input = new Dictionary<FuzzyVariable, double>();
                input.Add(rateOfError, nRateOfE);
                input.Add(error, nError);

                var result = fuzzySystem.Calculate(input);

                var nKp = result[param];
                return MathUtils.RestoreFromNormalized(nKp, kpMin, kpMax);

            } catch (Exception)
            {
                throw new Exception(string.Format("error = {0}, rateOfError = {1}", nError, nRateOfE));
            }
        }        

        public static double ComputeCoeffSugeno(SugenoFuzzySystem fuzzySystem, double nRateOfE, double nError, string nameOfCoeff, double kpMin, double kpMax)
        {
            var error = fuzzySystem.InputByName("error");
            var rateOfError = fuzzySystem.InputByName("rateOfError");
            var param = fuzzySystem.OutputByName(nameOfCoeff);

            var input = new Dictionary<FuzzyVariable, double>();
            input.Add(rateOfError, nRateOfE);
            input.Add(error, nError);

            var result = fuzzySystem.Calculate(input);
            var nKp = result[param];
            return MathUtils.RestoreFromNormalizedZeroOne(nKp, kpMin, kpMax);
        }        
        public static MamdaniFuzzySystem CreateMamSys(string coeffname, string[] rules)
        {
            MamdaniFuzzySystem fuzzySystem = new MamdaniFuzzySystem();
            var error = new FuzzyVariable("error", -1, 1);
            error.Terms.Add(new FuzzyTerm("NB", MamdaniMembershipFuncsForInp.NB));
            error.Terms.Add(new FuzzyTerm("NM", MamdaniMembershipFuncsForInp.NM));
            error.Terms.Add(new FuzzyTerm("NS", MamdaniMembershipFuncsForInp.NS));
            error.Terms.Add(new FuzzyTerm("ZO", MamdaniMembershipFuncsForInp.ZO));
            error.Terms.Add(new FuzzyTerm("PS", MamdaniMembershipFuncsForInp.PS));
            error.Terms.Add(new FuzzyTerm("PM", MamdaniMembershipFuncsForInp.PM));
            error.Terms.Add(new FuzzyTerm("PB", MamdaniMembershipFuncsForInp.PB));
            fuzzySystem.Input.Add(error);

            var rateOfError = new FuzzyVariable("rateOfError", -1, 1);
            rateOfError.Terms.Add(new FuzzyTerm("NB", MamdaniMembershipFuncsForInp.NB));
            rateOfError.Terms.Add(new FuzzyTerm("NM", MamdaniMembershipFuncsForInp.NM));
            rateOfError.Terms.Add(new FuzzyTerm("NS", MamdaniMembershipFuncsForInp.NS));
            rateOfError.Terms.Add(new FuzzyTerm("ZO", MamdaniMembershipFuncsForInp.ZO));
            rateOfError.Terms.Add(new FuzzyTerm("PS", MamdaniMembershipFuncsForInp.PS));
            rateOfError.Terms.Add(new FuzzyTerm("PM", MamdaniMembershipFuncsForInp.PM));
            rateOfError.Terms.Add(new FuzzyTerm("PB", MamdaniMembershipFuncsForInp.PB));
            fuzzySystem.Input.Add(rateOfError);

            var param = new FuzzyVariable(coeffname, -1, 1);
            param.Terms.Add(new FuzzyTerm("VVS", MamdaniMembershipFuncsForOut.VVS));
            param.Terms.Add(new FuzzyTerm("VS", MamdaniMembershipFuncsForOut.VS));
            param.Terms.Add(new FuzzyTerm("S", MamdaniMembershipFuncsForOut.S));
            param.Terms.Add(new FuzzyTerm("M", MamdaniMembershipFuncsForOut.M));
            param.Terms.Add(new FuzzyTerm("B", MamdaniMembershipFuncsForOut.B));
            param.Terms.Add(new FuzzyTerm("VB", MamdaniMembershipFuncsForOut.VB));
            param.Terms.Add(new FuzzyTerm("VVB", MamdaniMembershipFuncsForOut.VVB));
            fuzzySystem.Output.Add(param);
            
            foreach (string rule in rules)            
                fuzzySystem.Rules.Add(fuzzySystem.ParseRule(String.Format(rule, "rateOfError", "error", coeffname)));            
            return fuzzySystem;
        }
        
        public static SugenoFuzzySystem CreateSugenoSysForKpKi(string coeffname, string[] rules)
        {
            var fuzzySystem = new SugenoFuzzySystem();
            var error = new FuzzyVariable("error", -1, 1);
            error.Terms.Add(new FuzzyTerm("NB", MamdaniMembershipFuncsForInp.NB));
            error.Terms.Add(new FuzzyTerm("NM", MamdaniMembershipFuncsForInp.NM));
            error.Terms.Add(new FuzzyTerm("NS", MamdaniMembershipFuncsForInp.NS));
            error.Terms.Add(new FuzzyTerm("ZO", MamdaniMembershipFuncsForInp.ZO));
            error.Terms.Add(new FuzzyTerm("PS", MamdaniMembershipFuncsForInp.PS));
            error.Terms.Add(new FuzzyTerm("PM", MamdaniMembershipFuncsForInp.PM));
            error.Terms.Add(new FuzzyTerm("PB", MamdaniMembershipFuncsForInp.PB));
            fuzzySystem.Input.Add(error);

            var rateOfError = new FuzzyVariable("rateOfError", -1, 1);
            rateOfError.Terms.Add(new FuzzyTerm("NB", MamdaniMembershipFuncsForInp.NB));
            rateOfError.Terms.Add(new FuzzyTerm("NM", MamdaniMembershipFuncsForInp.NM));
            rateOfError.Terms.Add(new FuzzyTerm("NS", MamdaniMembershipFuncsForInp.NS));
            rateOfError.Terms.Add(new FuzzyTerm("ZO", MamdaniMembershipFuncsForInp.ZO));
            rateOfError.Terms.Add(new FuzzyTerm("PS", MamdaniMembershipFuncsForInp.PS));
            rateOfError.Terms.Add(new FuzzyTerm("PM", MamdaniMembershipFuncsForInp.PM));
            rateOfError.Terms.Add(new FuzzyTerm("PB", MamdaniMembershipFuncsForInp.PB));
            fuzzySystem.Input.Add(rateOfError);

            var param = new SugenoVariable(coeffname);
            param.Functions.Add(fuzzySystem.CreateSugenoFunction("S", new double[] { 0, 0, SugenoMembershipFuncsForOutKpKi.S}));
            param.Functions.Add(fuzzySystem.CreateSugenoFunction("B", new double[] { 0, 0, SugenoMembershipFuncsForOutKpKi.B}));
            fuzzySystem.Output.Add(param);
            
            foreach (string rule in rules)            
                fuzzySystem.Rules.Add(fuzzySystem.ParseRule(String.Format(rule, "error", "rateOfError", coeffname)));            
            return fuzzySystem;
        }

        public static SugenoFuzzySystem CreateSugenoSysForKd(string coeffname, string[] rules)
        {
            var fuzzySystem = new SugenoFuzzySystem();
            var error = new FuzzyVariable("error", -1, 1);
            error.Terms.Add(new FuzzyTerm("NB", MamdaniMembershipFuncsForInp.NB));
            error.Terms.Add(new FuzzyTerm("NM", MamdaniMembershipFuncsForInp.NM));
            error.Terms.Add(new FuzzyTerm("NS", MamdaniMembershipFuncsForInp.NS));
            error.Terms.Add(new FuzzyTerm("ZO", MamdaniMembershipFuncsForInp.ZO));
            error.Terms.Add(new FuzzyTerm("PS", MamdaniMembershipFuncsForInp.PS));
            error.Terms.Add(new FuzzyTerm("PM", MamdaniMembershipFuncsForInp.PM));
            error.Terms.Add(new FuzzyTerm("PB", MamdaniMembershipFuncsForInp.PB));
            fuzzySystem.Input.Add(error);

            var rateOfError = new FuzzyVariable("rateOfError", -1, 1);
            rateOfError.Terms.Add(new FuzzyTerm("NB", MamdaniMembershipFuncsForInp.NB));
            rateOfError.Terms.Add(new FuzzyTerm("NM", MamdaniMembershipFuncsForInp.NM));
            rateOfError.Terms.Add(new FuzzyTerm("NS", MamdaniMembershipFuncsForInp.NS));
            rateOfError.Terms.Add(new FuzzyTerm("ZO", MamdaniMembershipFuncsForInp.ZO));
            rateOfError.Terms.Add(new FuzzyTerm("PS", MamdaniMembershipFuncsForInp.PS));
            rateOfError.Terms.Add(new FuzzyTerm("PM", MamdaniMembershipFuncsForInp.PM));
            rateOfError.Terms.Add(new FuzzyTerm("PB", MamdaniMembershipFuncsForInp.PB));
            fuzzySystem.Input.Add(rateOfError);

            var param = new SugenoVariable(coeffname);
            param.Functions.Add(fuzzySystem.CreateSugenoFunction("ZO", new double[] { 0, 0, SugenoMembershipFuncsForOutKd.ZO}));
            param.Functions.Add(fuzzySystem.CreateSugenoFunction("S", new double[] { 0, 0, SugenoMembershipFuncsForOutKd.S}));
            param.Functions.Add(fuzzySystem.CreateSugenoFunction("M", new double[] { 0, 0, SugenoMembershipFuncsForOutKd.M}));
            param.Functions.Add(fuzzySystem.CreateSugenoFunction("B", new double[] { 0, 0, SugenoMembershipFuncsForOutKd.B}));
            fuzzySystem.Output.Add(param);
            
            foreach (string rule in rules)            
                fuzzySystem.Rules.Add(fuzzySystem.ParseRule(String.Format(rule, "error", "rateOfError", coeffname)));            
            return fuzzySystem;
        }

        public static double ConvertValUsingMinMax(double val, double min, double max, double delta=1e-4)
        {
            var converted = val;
            delta = delta * (max - min);
            if (val < min)
                converted = min + delta;
            else if (val > max)
                converted = max - delta;
            return converted;
        }

        public static double Sign(double number)
        {
            var result = number >= 0 ? 1: -1;
            return result;
        }
    }
}
