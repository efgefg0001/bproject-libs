﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    public class Motor
    {
        private double curVoltage;
        private double startVoltage;

        public double MinVoltage { get; set; }
        public double MaxVoltage { get; set; }

        public double MinOmega { get { return MinVoltage * Coeff; } }
        public double MaxOmega { get { return MaxVoltage * Coeff; } }

        public double Coeff { get; set; }

        public void Reset()
        {
            CurOmega = StartOmega;
        }

        public double StartOmega
        {
            get { return Coeff * StartVoltage; }
            set { StartVoltage = value / Coeff; }
        }
        public double CurOmega
        {
            get { return Coeff * CurVoltage; }
            set { CurVoltage = value / Coeff; }
        }

        public double StartVoltage
        {
            get { return startVoltage; }
            set
            {
                startVoltage = MathUtils.ConvertValUsingMinMax(value, MinVoltage, MaxVoltage, 0);
                curVoltage = startVoltage;
            } 
        }
        public double CurVoltage
        {
            get { return curVoltage; }
            set { curVoltage = MathUtils.ConvertValUsingMinMax(value, MinVoltage, MaxVoltage, 0); }
        }

    }
}
