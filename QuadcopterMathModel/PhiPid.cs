﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    class PhiPid : AbstractQuadcopterPid
    {
        public override double Calc(QuadcopterParams quadcopter, params double[] args)
        {
            var curPhi = args[0];
            var dt = args[1];
            var curVphi = args[2];
            var curT = args[3];
            CurError = quadcopter.DestAttitude.PhiRadians - curPhi;

            var rateOfError = CalcRateOfError(curVphi);//(CurError - PrevError) / dt;

            var res = (
                Kd * /*(- curVphi)*/rateOfError
                + this.Ki * MathUtils.TrapezoidIntegration((t) => CurError, quadcopter.TMin, curT)
                + Kp * CurError) * quadcopter.Jx;
            return res;
        }
    }
}
