﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    public class QuadcopterParams
    {
        public readonly double g = 9.81;

        public Point3d StartPosition { get; set; }
        public Point3d StartVelocity { get; set; }

        public Angle3d StartAttitude { get; set; }
        public Angle3d StartAttitudeRate { get; set; }

        public Angle3d DestAttitude { get; set; }
        public double Zdest { get; set; }
/*
        public double PhiDest { get; set; }
        public double ThetaDest { get; set; }
        public double PsiDest { get; set; }
*/
        public double k { get; set; }
        public double l { get; set; }
        public double b { get; set; }
        public double c_d { get; set; }
        public double rho { get; set; }
        public double S_x { get; set; }
        public double S_y { get; set; }
        public double S_z { get; set; }
        
        /// <summary>
        /// Масса центрального шара
        /// </summary>
        public double Ms { get; set; }

        /// <summary>
        /// Радиус центрального шара
        /// </summary>
        public double Rs { get; set; }

        /// <summary>
        /// Масса одного винта
        /// </summary>
        public double m { get; set; }

        private double jX;        
        /// <summary>
        /// Вычисление компоненты тензора инерции J[0,0] по оси x
        /// </summary>
        public double Jx
        {
            get
            {
                var res = 2 / 5 * Ms * Rs * Rs + 2 * l * l * m;
                return res;
                //return jX;
            }
            set
            {
                jX = value;
            }
        }

        private double jY;
        /// <summary>
        /// Вычисление компоненты тензора инерции J[1,1] по оси y
        /// </summary>
        public double Jy
        {
            get
            {
                var res = 2/5 * Ms * Rs * Rs + 2 * l * l * m;
                return res;
                //return jY;
            }
            set
            {
                jY = value;
            }
        }

        private double jZ;
        /// <summary>
        /// Вычисление компоненты тензора инерции J[2,2] по оси z
        /// </summary>
        public double Jz
        {
            get
            {
                var res = 2/5 * Ms * Rs * Rs + 4 * l * l * m;
                return res;
                //return jZ;
            }
            set
            {
                jZ = value;
            }
        }
        private double jR;
        public double Jr
        {
            get
            {
                //var res = m * l * l;
                //return res;
                return jR;
            }
            set
            {
                jR = value;
            }
        }

        public Motor Motor1 { get; set; }
        public Motor Motor2 { get; set; }
        public Motor Motor3 { get; set; }
        public Motor Motor4 { get; set; }
        
        public double CalcGyrosX(double omega1, double omega2, double omega3, double omega4)
        {
            var res = omega1 + omega3 - omega2 - omega4;
            return res;
        }

        public double CalcGyrosY(double omega1, double omega2, double omega3, double omega4)
        {
            var res = omega2 + omega4 - omega1 - omega3;
            return res;
        }

        /// <summary>
        /// Крутящий момент по крену (крен - угол phi)
        /// </summary>
        public double CalcTauPhi(params double[] args)
        {
            double omega2 = args[0], omega4 = args[1];
            double res = l * k * (omega4*omega4 - omega2*omega2);
            return res;
        }

        /// <summary>
        /// Крутящий момент по тангажу (тангаж - угол theta)
        /// </summary>
        public double CalcTauTheta(params double[] args)
        {
            double omega1 = args[0], omega3 = args[1];
            double res = l * k * (omega3*omega3 - omega1*omega1);
            return res;
        }

        /// <summary>
        /// Крутящий момент по рысканию (рыскание - угол psi) 
        /// </summary>
        public double CalcTauPsi(params double[] args)
        {
            double omega1 = args[0], omega2 = args[1], omega3 = args[2], omega4 = args[3];
            double res = b * (-omega1*omega1 + omega2 * omega2 - omega3*omega3 + omega4*omega4);
            return res;
        }
        
        /// <summary>
        /// Общая масса аппарата
        /// </summary>
        public double Mt
        {
            get
            {
                double res = Ms + 4 * m;
                return res;
            }
        }
        
        /// <summary>
        /// Сила тяги
        /// </summary>
        public double CalcF(params double[] args)
        {
            double omega1 = args[0], omega2 = args[1], omega3 = args[2], omega4 = args[3];
            double result = k * (omega1*omega1 + omega2*omega2 + omega3*omega3 + omega4*omega4);
            return result;
        }

        /// <summary>
        /// Система обыкновенных дифференциальных уравнений, описывающих состояние аппарата
        /// </summary>
        public Func<double, double[], QuadcopterOdeSolver, double>[] Funcs { get; set; }
        
        /// <summary>
        /// Исходные значения для дифференциальных уравнений при начальном t
        /// </summary>
//        public double[] InitialValsForFuncs { get; set; }



        /// <summary>
        /// Начальное значение времени, при котором заданы начальные значения для диф. уравнений
        /// </summary>
        public double TMin { get; set; }
    }
}
