﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    /// <summary>
    /// Результат решения системы обыкновенных диф. уравнений для квадрокоптера
    /// </summary>
    public class QuadcopterOdeResult
    {
        public IList<double> T { get; set; }
        public IList<double[]> X { get; set; }
        public IList<double[]> Omega { get; set; }
    }
}
