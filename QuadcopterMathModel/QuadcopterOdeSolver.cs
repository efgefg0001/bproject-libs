﻿#define DEBUG

using System;
using System.Collections.Generic;
using NLog;


namespace QuadcopterMathModel
{
    public class TimeParams
    {
        private double tMax;
        /// <summary>
        /// Максимальное время
        /// </summary>
        public double TMax
        {
            get
            {
                return /*2**/tMax;
            }
            set
            {
                tMax = value;
            }
        }

        public double TMaxH
        {
            get
            {
                return tMax / 2;
            }
            set
            {
                tMax = 2 * value;
            }
        }

        /// <summary>
        /// Шаг по времени
        /// </summary>
        public double H { get; set; }
    }

    public class QuadcopterOdeSolver
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public QuadcopterOdeSolver()
        {
            UsePidRegulation = true;
        }

        public bool UsePidRegulation { get; set; }

        public string Description { get; set; }
        public override string ToString()
        {
            return Description;
        }
        /// <summaryStart>
        /// Параметры квадрокоптера
        /// </summary>
        public QuadcopterParams Quadcopter { get; set; }

        public TimeParams Time { get; set; }

        /// <summary>
        /// Пид для контроля высоты по z
        /// </summary>
        public AbstractQuadcopterPid ZPid { get; set; }

        /// <summary>
        /// Пид для контроля крена, phi - крен
        /// </summary>
        public AbstractQuadcopterPid PhiPid { get; set; }

        /// <summary>
        /// Пид для контроля тангажа, theta - тангаж 
        /// </summary>
        public AbstractQuadcopterPid ThetaPid { get; set; }

        /// <summary>
        /// Пид для контроля рыскания, psi - рыскание
        /// </summary>
        public AbstractQuadcopterPid PsiPid { get; set; }
        
        /// <summary>
        /// Вычисление коэффициетов a для формулы Рунге-Кутты
        /// </summary>
        /// <param name="t">Текущее значение переменной, по которой идёт дифференцирование</param>
        /// <param name="xVec">Значения переменных, по которым не проходит дифференцирование</param>
        /// <param name="aVec">Значение коэффициента a</param>
        private void ComputeA(double t, double[] xVec, ref double[] aVec)
        {
            for (int i = 0, count = aVec.Length; i < count; ++i)
                aVec[i] = Quadcopter.Funcs[i](t, xVec, this);
        }
        
        /// <summary>
        /// Вычисление аргументов для диф. уравнения, используя пар-ры, по которым мы не дифференцируем
        /// </summary>
        private double[] ComputeSecondArgForF(double[] xVec, double[] secondVec, double step)
        {
            var xVecPlusSecondVec = new double[xVec.Length];
            for (int i = 0, count = xVecPlusSecondVec.Length; i < count; ++i)
                xVecPlusSecondVec[i] = xVec[i] + step * secondVec[i];
            return xVecPlusSecondVec;
        }

        private double halfH;

        /// <summary>
        /// Обобщённая формула для вычисления коэффициентов b, c, d
        /// для формулы Рунге-Кутты
        /// </summary>
        /// <param name="t">Текущее значение переменной, по которой идёт дифференцирование</param>
        /// <param name="step">Шаг по t</param>
        /// <param name="xVec">Вектор значений переменных в диф. уравнениях, по которым не идёт дифференцирование</param>
        /// <param name="secondVec">Значения предыдущего коэфф-та для расчёта b, c, d</param>
        /// <param name="resultVec">Результирующий коэффициент</param>
        private void ComputeCoeff(double t, double step, double[] xVec, double[] secondVec, ref double[] resultVec)
        {
            var xVecPlusSecondVec = ComputeSecondArgForF(xVec, secondVec, step);
            for (int i = 0, count = resultVec.Length; i < count; ++i)
                resultVec[i] = Quadcopter.Funcs[i](t + halfH, xVecPlusSecondVec, this);
        }

        /// <summary>
        /// Вычислить следующее значение вектора x
        /// </summary>
        /// <param name="curXVec">Текущее значение вектора x</param>
        /// <param name="stepFactor">Значение h/6 в формуле</param>
        /// <param name="aVec">Вектор a</param>
        /// <param name="bVec">Вектор b</param>
        /// <param name="cVec">Вектор c</param>
        /// <returns>Следующее значение вектора x</returns>
        private double[] ComputeNextXVec(double[] curXVec, double stepFactor, double[] aVec, double[] bVec, double[] cVec, double[] dVec)
        {
            var nextXVec = new double[curXVec.Length];
            for (int i =0, count = nextXVec.Length; i < count; ++i)
                nextXVec[i] = curXVec[i] + stepFactor * (aVec[i] + 2.0 * (bVec[i] + cVec[i]) + dVec[i]);
            return nextXVec;
        }

        private void ResetMotors()
        {
            Quadcopter.Motor1.Reset(); Quadcopter.Motor2.Reset();
            Quadcopter.Motor3.Reset(); Quadcopter.Motor4.Reset();
        }

        /// <summary>
        /// Решение оду для квадрокоптера явным методом Рунге-Кутты 4 порядка
        /// </summary>
        /// <returns>Время, параметы положения, угловые скорости винтов</returns>
        public QuadcopterOdeResult Solve()
        {
//            ResetSolver();
            halfH = 0.5 * Time.H;
            var hDividedBySix = Time.H / 6;

            double[] aVec = new double[Quadcopter.Funcs.Length], bVec = new double[Quadcopter.Funcs.Length], cVec = new double[Quadcopter.Funcs.Length],
                dVec = new double[Quadcopter.Funcs.Length];

            double[] curXVec = new double[] 
            {
                Quadcopter.StartPosition.X, Quadcopter.StartVelocity.X,
                Quadcopter.StartPosition.Y, Quadcopter.StartVelocity.Y,
                Quadcopter.StartPosition.Z, Quadcopter.StartVelocity.Z,
                Quadcopter.StartAttitude.PhiRadians, Quadcopter.StartAttitudeRate.PhiRadians,
                Quadcopter.StartAttitude.ThetaRadians, Quadcopter.StartAttitudeRate.ThetaRadians,
                Quadcopter.StartAttitude.PsiRadians, Quadcopter.StartAttitudeRate.PsiRadians,
            };

            var allXVectors = new List<double[]> { curXVec };
            var t = new List<double> { Quadcopter.TMin };

            ResetMotors();

//            CurOmega1 = Quadcopter.Motor1.StartOmega; CurOmega2 = Quadcopter.Motor2.StartOmega;
//            CurOmega3 = Quadcopter.Motor3.StartOmega; CurOmega4 = Quadcopter.Motor4.StartOmega;

            var omega = new List<double[]>() {
                new double[] { Quadcopter.Motor1.CurOmega, Quadcopter.Motor2.CurOmega, Quadcopter.Motor3.CurOmega, Quadcopter.Motor4.CurOmega }
            };

            var initialIteration = false;//true;
            ComputeInitialErrors();
            for (double curT = Quadcopter.TMin; curT <= Time.TMax; curT += Time.H)
            {
//                currentT = curT + Time.H;
                if (UsePidRegulation && !initialIteration) 
                    PidRegulation(curT + Time.H, curXVec);

                ComputeA(curT, curXVec, ref aVec);
                ComputeCoeff(curT, halfH, curXVec, aVec, ref bVec);
                ComputeCoeff(curT, halfH, curXVec, bVec, ref cVec);
                ComputeCoeff(curT, Time.H, curXVec, cVec, ref dVec);
                curXVec = ComputeNextXVec(curXVec, hDividedBySix, aVec, bVec, cVec, dVec);

                t.Add(0.5*(curT + Time.H));
                allXVectors.Add(curXVec);
                omega.Add(new double[] {
                    Quadcopter.Motor1.CurOmega, Quadcopter.Motor2.CurOmega, Quadcopter.Motor3.CurOmega, Quadcopter.Motor4.CurOmega
                });

                if (UsePidRegulation && initialIteration)
                    initialIteration = false;
            }
            return new QuadcopterOdeResult { T = t, X = allXVectors, Omega = omega };
        }

        private void ComputeInitialErrors()
        {
            ZPid.PrevError = Quadcopter.Zdest - Quadcopter.StartPosition.Z;
            PhiPid.PrevError = Quadcopter.DestAttitude.PhiRadians - Quadcopter.StartAttitude.PhiRadians;
            ThetaPid.PrevError = Quadcopter.DestAttitude.ThetaRadians - Quadcopter.StartAttitude.ThetaRadians;
            PsiPid.PrevError = Quadcopter.DestAttitude.PsiRadians - Quadcopter.StartAttitude.PsiRadians;
        }
        
        private void PidRegulation(double curT, double[] curXVec)
        {
            var F = ZPid.Calc(Quadcopter, curXVec[4], curXVec[6], curXVec[8], Time.H, curXVec[5], curT);
            var tauPhi = PhiPid.Calc(Quadcopter, curXVec[6], Time.H, curXVec[7], curT);
            var tauTheta = ThetaPid.Calc(Quadcopter, curXVec[8], Time.H, curXVec[9], curT);
            var tauPsi = PsiPid.Calc(Quadcopter, curXVec[10], Time.H, curXVec[11], curT);

            UpdateAngularVelocities(F, tauPhi, tauTheta, tauPsi);

            ZPid.PrevError = ZPid.CurError;
            PhiPid.PrevError = PhiPid.CurError;
            ThetaPid.PrevError = ThetaPid.CurError;
            PsiPid.PrevError = PsiPid.CurError;
        }

        private void UpdateAngularVelocities(double inpF, double tauPhi, double tauTheta, double tauPsi)
        {
            var F = inpF;
            logger.Debug(string.Format("F={0}, tauPhi={1}, tauTheta={2}, tauPsi={3}", F, tauPhi, tauTheta, tauPsi));
            double temp = 0.0;
            temp = F / (4 * Quadcopter.k) - tauTheta / (2 * Quadcopter.k * Quadcopter.l) - tauPsi / (4 * Quadcopter.b);
            logger.Debug("tempOmega1 = " + temp);

            if (temp >= 0.0)
                Quadcopter.Motor1.CurOmega = Math.Sqrt(temp);
            temp = F / (4 * Quadcopter.k) - tauPhi / (2 * Quadcopter.k * Quadcopter.l) + tauPsi / (4 * Quadcopter.b);
            logger.Debug("tempOmega2 = " + temp);
            if (temp >= 0.0)
                Quadcopter.Motor2.CurOmega = Math.Sqrt(temp);
            temp = F / (4 * Quadcopter.k) + tauTheta / (2 * Quadcopter.k * Quadcopter.l) - tauPsi / (4 * Quadcopter.b);
            logger.Debug("tempOmega3 = " + temp);
            if (temp >= 0.0)
                Quadcopter.Motor3.CurOmega = Math.Sqrt(temp);
            temp = F / (4 * Quadcopter.k) + tauPhi / (2 * Quadcopter.k * Quadcopter.l) + tauPsi / (4 * Quadcopter.b);
            logger.Debug("tempOmega4 = " + temp);
            if (temp >= 0.0)
                Quadcopter.Motor4.CurOmega = Math.Sqrt(temp);
        }

//        private double F;
//        private double tauPhi, tauTheta, tauPsi;

//        private double curZ, curPhi, curTheta, curPsi;
//        private double curVz, curVphi, curVtheta, curVpsi;

//        public double CurOmega1, CurOmega2, CurOmega3, CurOmega4;

//        private double currentT;

        private void ResetSolver()
        {
            halfH = 0;
//            F = 0;
//            tauPhi = 0; tauTheta = 0; tauPsi = 0;
//            curZ = 0; curPhi = 0; curTheta = 0; curPsi = 0;
//            curVz = 0; curVphi = 0; curVtheta = 0; curVpsi = 0;
//            currentT = 0;
        }
    }
}
