﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FuzzyLogic;

namespace QuadcopterMathModel
{
    static class MamdaniMembershipFuncsForInp
    {
        public static TriangularMembershipFunction NB
        {
            get
            {
                return new TriangularMembershipFunction(-1, -1, -0.6);
            }
        }

        public static TriangularMembershipFunction NM
        {
            get
            {
                return new TriangularMembershipFunction(-1, -0.6, 0);
            }
        }

        public static TriangularMembershipFunction NS
        {
            get
            {
                return new TriangularMembershipFunction(-1, -0.4, 0);
            }
        }

        public static TriangularMembershipFunction ZO
        {
            get
            {
                return new TriangularMembershipFunction(-0.3, 0, 0.3);
            }
        }

        public static TriangularMembershipFunction PS
        {
            get
            {
                return new TriangularMembershipFunction(0, 0.4, 1);
            }
        }

        public static TriangularMembershipFunction PM
        {
            get
            {
                return new TriangularMembershipFunction(0, 0.6, 1);
            }
        }

        public static TriangularMembershipFunction PB
        {
            get
            {
                return new TriangularMembershipFunction(0.6, 1, 1);
            }
        }

    }

    static class MamdaniMembershipFuncsForOut
    {
        public static TriangularMembershipFunction VVS
        {
            get
            {
                return new TriangularMembershipFunction(-1, -1, -0.6);
            }
        }

        public static TriangularMembershipFunction VS
        {
            get
            {
                return new TriangularMembershipFunction(-1, -0.6, 0);
            }
        }

        public static TriangularMembershipFunction S
        {
            get
            {
                return new TriangularMembershipFunction(-1, -0.4, 0);
            }
        }

        public static TriangularMembershipFunction M
        {
            get
            {
                return new TriangularMembershipFunction(-0.3, 0, 0.3);
            }
        }

        public static TriangularMembershipFunction B
        {
            get
            {
                return new TriangularMembershipFunction(0, 0.4, 1);
            }
        }

        public static TriangularMembershipFunction VB
        {
            get
            {
                return new TriangularMembershipFunction(0, 0.6, 1);
            }
        }

        public static TriangularMembershipFunction VVB
        {
            get
            {
                return new TriangularMembershipFunction(0.6, 1, 1);
            }
        }

    }

    static class MamdaniPidRules
    {
        public static readonly string[] KpKiRules = 
        {
            "if ({0} is NB) and ({1} is NB) then ({2} is M)",
            "if ({0} is NB) and ({1} is NM) then ({2} is S)",
            "if ({0} is NB) and ({1} is NS) then ({2} is VS)",
            "if ({0} is NB) and ({1} is ZO) then ({2} is VVS)",
            "if ({0} is NB) and ({1} is PS) then ({2} is VS)",
            "if ({0} is NB) and ({1} is PM) then ({2} is S)",
            "if ({0} is NB) and ({1} is PB) then ({2} is M)",

            "if ({0} is NM) and ({1} is NB) then ({2} is B)",
            "if ({0} is NM) and ({1} is NM) then ({2} is M)",
            "if ({0} is NM) and ({1} is NS) then ({2} is S)",
            "if ({0} is NM) and ({1} is ZO) then ({2} is VS)",
            "if ({0} is NM) and ({1} is PS) then ({2} is S)",
            "if ({0} is NM) and ({1} is PM) then ({2} is M)",
            "if ({0} is NM) and ({1} is PB) then ({2} is B)",

            "if ({0} is NS) and ({1} is NB) then ({2} is VB)",
            "if ({0} is NS) and ({1} is NM) then ({2} is B)",
            "if ({0} is NS) and ({1} is NS) then ({2} is M)",
            "if ({0} is NS) and ({1} is ZO) then ({2} is S)",
            "if ({0} is NS) and ({1} is PS) then ({2} is M)",
            "if ({0} is NS) and ({1} is PM) then ({2} is B)",
            "if ({0} is NS) and ({1} is PB) then ({2} is VB)",

            "if ({0} is ZO) and ({1} is NB) then ({2} is VVB)",
            "if ({0} is ZO) and ({1} is NM) then ({2} is VB)",
            "if ({0} is ZO) and ({1} is NS) then ({2} is B)",
            "if ({0} is ZO) and ({1} is ZO) then ({2} is M)",
            "if ({0} is ZO) and ({1} is PS) then ({2} is B)",
            "if ({0} is ZO) and ({1} is PM) then ({2} is VB)",
            "if ({0} is ZO) and ({1} is PB) then ({2} is VVB)",

            "if ({0} is PS) and ({1} is NB) then ({2} is VB)",
            "if ({0} is PS) and ({1} is NM) then ({2} is B)",
            "if ({0} is PS) and ({1} is NS) then ({2} is M)",
            "if ({0} is PS) and ({1} is ZO) then ({2} is S)",
            "if ({0} is PS) and ({1} is PS) then ({2} is M)",
            "if ({0} is PS) and ({1} is PM) then ({2} is B)",
            "if ({0} is PS) and ({1} is PB) then ({2} is VB)",

            "if ({0} is PM) and ({1} is NB) then ({2} is B)",
            "if ({0} is PM) and ({1} is NM) then ({2} is M)",
            "if ({0} is PM) and ({1} is NS) then ({2} is S)",
            "if ({0} is PM) and ({1} is ZO) then ({2} is VS)",
            "if ({0} is PM) and ({1} is PS) then ({2} is S)",
            "if ({0} is PM) and ({1} is PM) then ({2} is M)",
            "if ({0} is PM) and ({1} is PB) then ({2} is B)",

            "if ({0} is PB) and ({1} is NB) then ({2} is M)",
            "if ({0} is PB) and ({1} is NM) then ({2} is S)",
            "if ({0} is PB) and ({1} is NS) then ({2} is VS)",
            "if ({0} is PB) and ({1} is ZO) then ({2} is VVS)",
            "if ({0} is PB) and ({1} is PS) then ({2} is VS)",
            "if ({0} is PB) and ({1} is PM) then ({2} is S)",
            "if ({0} is PB) and ({1} is PB) then ({2} is M)",
        };

// сначала идет rateOfError, потом error
        public static readonly string[] KdRules = 
        {
            "if ({0} is NB) and ({1} is NB) then ({2} is M)",
            "if ({0} is NB) and ({1} is NM) then ({2} is B)",
            "if ({0} is NB) and ({1} is NS) then ({2} is VB)",
            "if ({0} is NB) and ({1} is ZO) then ({2} is VVB)",
            "if ({0} is NB) and ({1} is PS) then ({2} is VB)",
            "if ({0} is NB) and ({1} is PM) then ({2} is B)",
            "if ({0} is NB) and ({1} is PB) then ({2} is M)",

            "if ({0} is NM) and ({1} is NB) then ({2} is S)",
            "if ({0} is NM) and ({1} is NM) then ({2} is M)",
            "if ({0} is NM) and ({1} is NS) then ({2} is B)",
            "if ({0} is NM) and ({1} is ZO) then ({2} is VB)",
            "if ({0} is NM) and ({1} is PS) then ({2} is B)",
            "if ({0} is NM) and ({1} is PM) then ({2} is M)",
            "if ({0} is NM) and ({1} is PB) then ({2} is S)",

            "if ({0} is NS) and ({1} is NB) then ({2} is VS)",
            "if ({0} is NS) and ({1} is NM) then ({2} is S)",
            "if ({0} is NS) and ({1} is NS) then ({2} is M)",
            "if ({0} is NS) and ({1} is ZO) then ({2} is B)",
            "if ({0} is NS) and ({1} is PS) then ({2} is M)",
            "if ({0} is NS) and ({1} is PM) then ({2} is S)",
            "if ({0} is NS) and ({1} is PB) then ({2} is VS)",

            "if ({0} is ZO) and ({1} is NB) then ({2} is VVS)",
            "if ({0} is ZO) and ({1} is NM) then ({2} is VS)",
            "if ({0} is ZO) and ({1} is NS) then ({2} is S)",
            "if ({0} is ZO) and ({1} is ZO) then ({2} is M)",
            "if ({0} is ZO) and ({1} is PS) then ({2} is S)",
            "if ({0} is ZO) and ({1} is PM) then ({2} is VS)",
            "if ({0} is ZO) and ({1} is PB) then ({2} is VVS)",

            "if ({0} is PS) and ({1} is NB) then ({2} is VS)",
            "if ({0} is PS) and ({1} is NM) then ({2} is S)",
            "if ({0} is PS) and ({1} is NS) then ({2} is M)",
            "if ({0} is PS) and ({1} is ZO) then ({2} is B)",
            "if ({0} is PS) and ({1} is PS) then ({2} is M)",
            "if ({0} is PS) and ({1} is PM) then ({2} is S)",
            "if ({0} is PS) and ({1} is PB) then ({2} is VS)",

            "if ({0} is PM) and ({1} is NB) then ({2} is S)",
            "if ({0} is PM) and ({1} is NM) then ({2} is M)",
            "if ({0} is PM) and ({1} is NS) then ({2} is B)",
            "if ({0} is PM) and ({1} is ZO) then ({2} is VB)",
            "if ({0} is PM) and ({1} is PS) then ({2} is B)",
            "if ({0} is PM) and ({1} is PM) then ({2} is M)",
            "if ({0} is PM) and ({1} is PB) then ({2} is S)",

            "if ({0} is PB) and ({1} is NB) then ({2} is M)",
            "if ({0} is PB) and ({1} is NM) then ({2} is B)",
            "if ({0} is PB) and ({1} is NS) then ({2} is VB)",
            "if ({0} is PB) and ({1} is ZO) then ({2} is VVB)",
            "if ({0} is PB) and ({1} is PS) then ({2} is VB)",
            "if ({0} is PB) and ({1} is PM) then ({2} is B)",
            "if ({0} is PB) and ({1} is PB) then ({2} is M)",
        };
    }

    class MamdaniZPid : ZPid
    {
        private double errorMin = -10, errorMax = 10;
        private double rateOfErrorMin = -10, rateOfErrorMax = 10;

        private string kdName = "kd";
        private string kiName = "ki";
        private string kpName = "kp";

        private double KPMin = 0, KPMax = 10;
        private double KIMin = 0, KIMax = 10;
        private double KDMin = 0, KDMax = 10;

        private MamdaniFuzzySystem kPMamFuzSys;
        private MamdaniFuzzySystem kIMamFuzSys;
        private MamdaniFuzzySystem kDMamFuzSys;

        public MamdaniZPid()
        {
            kPMamFuzSys = MathUtils.CreateMamSys(kpName, MamdaniPidRules.KpKiRules);
            kIMamFuzSys = MathUtils.CreateMamSys(kiName, MamdaniPidRules.KpKiRules);
            kDMamFuzSys = MathUtils.CreateMamSys(kdName, MamdaniPidRules.KdRules);
        }


        public override double Calc(QuadcopterParams quadcopter, params double[] args)
        {
            var curZ = args[0];
            var curPhi = args[1];
            var curTheta = args[2];
            var dt = args[3];
            var curVz = args[4];
            var curT = args[5];

            CurError = MathUtils.ConvertValUsingMinMax(quadcopter.Zdest - curZ, errorMin, errorMax);
            var rateOfError = MathUtils.ConvertValUsingMinMax(
                CalcRateOfError(curVz),
                rateOfErrorMin, rateOfErrorMax);

            var normRateOfError = MathUtils.Normalize(rateOfError, rateOfErrorMin, rateOfErrorMax);
            var normError = MathUtils.Normalize(this.CurError, errorMin, errorMax);

            Kp = MathUtils.ComputeCoeff(kPMamFuzSys, normRateOfError, normError, kpName, KPMin, KPMax);
            Ki = MathUtils.ComputeCoeff(kIMamFuzSys, normRateOfError, normError, kiName, KIMin, KIMax);
            Kd = MathUtils.ComputeCoeff(kDMamFuzSys, normRateOfError, normError, kdName, KDMin, KDMax);

            var res =  /*Math.Sign(curVz) * quadcopter.c_d * 0.5 * quadcopter.rho * curVz * curVz * quadcopter.S_z / (Math.Cos(curPhi)*Math.Cos(curTheta))*/
                + (
                    MathUtils.g + this.Kd * rateOfError + this.Ki * MathUtils.TrapezoidIntegration((t) => CurError, quadcopter.TMin, curT)
                    + this.Kp * CurError
                ) * quadcopter.Mt / (Math.Cos(curPhi)*Math.Cos(curTheta));
            return res;
        }
    }
}
