﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FuzzyLogic;

namespace QuadcopterMathModel
{
    class MamdaniPhiPid : PhiPid
    {
        private double errorMin = -1, errorMax = 1;
        private double rateOfErrorMin = -10, rateOfErrorMax = 10;

        private string kdName = "kd";
        private string kiName = "ki";
        private string kpName = "kp";

        private double KPMin = 0, KPMax = 10;
        private double KIMin = 0, KIMax = 10;
        private double KDMin = 0, KDMax = 10;

        private MamdaniFuzzySystem kPMamFuzSys;
        private MamdaniFuzzySystem kIMamFuzSys;
        private MamdaniFuzzySystem kDMamFuzSys;

        public MamdaniPhiPid()
        {
            kPMamFuzSys = MathUtils.CreateMamSys(kpName, MamdaniPidRules.KpKiRules);
            kIMamFuzSys = MathUtils.CreateMamSys(kiName, MamdaniPidRules.KpKiRules);
            kDMamFuzSys = MathUtils.CreateMamSys(kdName, MamdaniPidRules.KdRules);
        }


        public override double Calc(QuadcopterParams quadcopter, params double[] args)
        {
            var curPhi = args[0];
            var dt = args[1];
            var curVphi = args[2];
            var curT = args[3];

            CurError = MathUtils.ConvertValUsingMinMax(quadcopter.DestAttitude.PhiRadians - curPhi, errorMin, errorMax);
            var rateOfError = MathUtils.ConvertValUsingMinMax(
                CalcRateOfError(curVphi),
                rateOfErrorMin, rateOfErrorMax);

            var normRateOfError = MathUtils.Normalize(rateOfError, rateOfErrorMin, rateOfErrorMax);
            var normError = MathUtils.Normalize(this.CurError, errorMin, errorMax);
            Kp = MathUtils.ComputeCoeff(kPMamFuzSys, normRateOfError, normError, kpName, KPMin, KPMax);
            Ki = MathUtils.ComputeCoeff(kIMamFuzSys, normRateOfError, normError, kiName, KIMin, KIMax);
            Kd = MathUtils.ComputeCoeff(kDMamFuzSys, normRateOfError, normError, kdName, KDMin, KDMax);

            var res = (
                Kd * rateOfError 
                + this.Ki * MathUtils.TrapezoidIntegration((t) => CurError, quadcopter.TMin, curT)
                + Kp * CurError) * quadcopter.Jx;
            return res;
        }
    }
}
