﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;

namespace QuadcopterMathModel
{
    class ZPid : AbstractQuadcopterPid
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public override double Calc(QuadcopterParams quadcopter, params double[] args)
        {
            var curZ = args[0];
            var curPhi = args[1];
            var curTheta = args[2];
            var dt = args[3];
            var curVz = args[4];
            var curT = args[5];

            CurError = quadcopter.Zdest - curZ;

            var rateOfError = CalcRateOfError(curVz);//(CurError - PrevError) / dt;

            var res =  /*Math.Sign(curVz) * quadcopter.c_d * 0.5 * quadcopter.rho * curVz * curVz * quadcopter.S_z / (Math.Cos(curPhi)*Math.Cos(curTheta))*/
                + (
                    MathUtils.g + this.Kd * rateOfError/*(-curVz)*/ + this.Ki * MathUtils.TrapezoidIntegration((t) => CurError, quadcopter.TMin, curT)
                    + this.Kp * CurError
                ) * quadcopter.Mt / (Math.Cos(curPhi)*Math.Cos(curTheta));
            //logger.Debug(string.Format(""));
            return res;
        }
    }
}
