﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    class ThetaPid : AbstractQuadcopterPid
    {
        public override double Calc(QuadcopterParams quadcopter, params double[] args)
        {
            var curTheta = args[0];
            var dt = args[1];
            var curVtheta = args[2];
            var curT = args[3];
            CurError = quadcopter.DestAttitude.ThetaRadians - curTheta;

            var rateOfError = CalcRateOfError(curVtheta);//(CurError - PrevError) / dt;

            var res = (
                Kd * rateOfError//(-curVtheta) 
                + this.Ki * MathUtils.TrapezoidIntegration((t) => CurError, quadcopter.TMin, curT)
                + Kp * CurError) * quadcopter.Jy;
            return res;
        }
    }
}
