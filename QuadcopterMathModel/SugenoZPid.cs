﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FuzzyLogic;


namespace QuadcopterMathModel
{
    static class SugenoMembershipFuncsForOutKpKi
    {
        public const double S = 0.1;
        public const double B = 0.9;
    }

    static class SugenoMembershipFuncsForOutKd
    {
        public const double ZO = 0;
        public const double S = 0.3;
        public const double M = 0.7;
        public const double B = 1;
    }
// сначала rateOfError, потом error
    static class SugenoPidRules
    {
        public static readonly string[] KpRules = 
        {
            "if ({0} is NB) and ({1} is NB) then ({2} is B)",
            "if ({0} is NB) and ({1} is NM) then ({2} is B)",
            "if ({0} is NB) and ({1} is NS) then ({2} is B)",
            "if ({0} is NB) and ({1} is ZO) then ({2} is B)",
            "if ({0} is NB) and ({1} is PS) then ({2} is B)",
            "if ({0} is NB) and ({1} is PM) then ({2} is B)",
            "if ({0} is NB) and ({1} is PB) then ({2} is B)",

            "if ({0} is NM) and ({1} is NB) then ({2} is S)",
            "if ({0} is NM) and ({1} is NM) then ({2} is B)",
            "if ({0} is NM) and ({1} is NS) then ({2} is B)",
            "if ({0} is NM) and ({1} is ZO) then ({2} is B)",
            "if ({0} is NM) and ({1} is PS) then ({2} is B)",
            "if ({0} is NM) and ({1} is PM) then ({2} is B)",
            "if ({0} is NM) and ({1} is PB) then ({2} is S)",

            "if ({0} is NS) and ({1} is NB) then ({2} is S)",
            "if ({0} is NS) and ({1} is NM) then ({2} is S)",
            "if ({0} is NS) and ({1} is NS) then ({2} is B)",
            "if ({0} is NS) and ({1} is ZO) then ({2} is B)",
            "if ({0} is NS) and ({1} is PS) then ({2} is B)",
            "if ({0} is NS) and ({1} is PM) then ({2} is S)",
            "if ({0} is NS) and ({1} is PB) then ({2} is S)",

            "if ({0} is ZO) and ({1} is NB) then ({2} is S)",
            "if ({0} is ZO) and ({1} is NM) then ({2} is S)",
            "if ({0} is ZO) and ({1} is NS) then ({2} is S)",
            "if ({0} is ZO) and ({1} is ZO) then ({2} is B)",
            "if ({0} is ZO) and ({1} is PS) then ({2} is S)",
            "if ({0} is ZO) and ({1} is PM) then ({2} is S)",
            "if ({0} is ZO) and ({1} is PB) then ({2} is S)",

            "if ({0} is PS) and ({1} is NB) then ({2} is S)",
            "if ({0} is PS) and ({1} is NM) then ({2} is S)",
            "if ({0} is PS) and ({1} is NS) then ({2} is B)",
            "if ({0} is PS) and ({1} is ZO) then ({2} is B)",
            "if ({0} is PS) and ({1} is PS) then ({2} is B)",
            "if ({0} is PS) and ({1} is PM) then ({2} is S)",
            "if ({0} is PS) and ({1} is PB) then ({2} is S)",

            "if ({0} is PM) and ({1} is NB) then ({2} is S)",
            "if ({0} is PM) and ({1} is NM) then ({2} is B)",
            "if ({0} is PM) and ({1} is NS) then ({2} is B)",
            "if ({0} is PM) and ({1} is ZO) then ({2} is B)",
            "if ({0} is PM) and ({1} is PS) then ({2} is B)",
            "if ({0} is PM) and ({1} is PM) then ({2} is B)",
            "if ({0} is PM) and ({1} is PB) then ({2} is S)",

            "if ({0} is PB) and ({1} is NB) then ({2} is B)",
            "if ({0} is PB) and ({1} is NM) then ({2} is B)",
            "if ({0} is PB) and ({1} is NS) then ({2} is B)",
            "if ({0} is PB) and ({1} is ZO) then ({2} is B)",
            "if ({0} is PB) and ({1} is PS) then ({2} is B)",
            "if ({0} is PB) and ({1} is PM) then ({2} is B)",
            "if ({0} is PB) and ({1} is PB) then ({2} is B)",
        };

        public static readonly string[] KiRules = 
        {
            "if ({0} is NB) and ({1} is NB) then ({2} is S)",
            "if ({0} is NB) and ({1} is NM) then ({2} is S)",
            "if ({0} is NB) and ({1} is NS) then ({2} is S)",
            "if ({0} is NB) and ({1} is ZO) then ({2} is S)",
            "if ({0} is NB) and ({1} is PS) then ({2} is S)",
            "if ({0} is NB) and ({1} is PM) then ({2} is S)",
            "if ({0} is NB) and ({1} is PB) then ({2} is S)",

            "if ({0} is NM) and ({1} is NB) then ({2} is B)",
            "if ({0} is NM) and ({1} is NM) then ({2} is B)",
            "if ({0} is NM) and ({1} is NS) then ({2} is S)",
            "if ({0} is NM) and ({1} is ZO) then ({2} is S)",
            "if ({0} is NM) and ({1} is PS) then ({2} is S)",
            "if ({0} is NM) and ({1} is PM) then ({2} is B)",
            "if ({0} is NM) and ({1} is PB) then ({2} is B)",

            "if ({0} is NS) and ({1} is NB) then ({2} is B)",
            "if ({0} is NS) and ({1} is NM) then ({2} is B)",
            "if ({0} is NS) and ({1} is NS) then ({2} is B)",
            "if ({0} is NS) and ({1} is ZO) then ({2} is S)",
            "if ({0} is NS) and ({1} is PS) then ({2} is B)",
            "if ({0} is NS) and ({1} is PM) then ({2} is B)",
            "if ({0} is NS) and ({1} is PB) then ({2} is B)",

            "if ({0} is ZO) and ({1} is NB) then ({2} is B)",
            "if ({0} is ZO) and ({1} is NM) then ({2} is B)",
            "if ({0} is ZO) and ({1} is NS) then ({2} is B)",
            "if ({0} is ZO) and ({1} is ZO) then ({2} is B)",
            "if ({0} is ZO) and ({1} is PS) then ({2} is B)",
            "if ({0} is ZO) and ({1} is PM) then ({2} is B)",
            "if ({0} is ZO) and ({1} is PB) then ({2} is B)",

            "if ({0} is PS) and ({1} is NB) then ({2} is B)",
            "if ({0} is PS) and ({1} is NM) then ({2} is B)",
            "if ({0} is PS) and ({1} is NS) then ({2} is B)",
            "if ({0} is PS) and ({1} is ZO) then ({2} is S)",
            "if ({0} is PS) and ({1} is PS) then ({2} is B)",
            "if ({0} is PS) and ({1} is PM) then ({2} is B)",
            "if ({0} is PS) and ({1} is PB) then ({2} is B)",

            "if ({0} is PM) and ({1} is NB) then ({2} is B)",
            "if ({0} is PM) and ({1} is NM) then ({2} is B)",
            "if ({0} is PM) and ({1} is NS) then ({2} is S)",
            "if ({0} is PM) and ({1} is ZO) then ({2} is S)",
            "if ({0} is PM) and ({1} is PS) then ({2} is S)",
            "if ({0} is PM) and ({1} is PM) then ({2} is B)",
            "if ({0} is PM) and ({1} is PB) then ({2} is B)",

            "if ({0} is PB) and ({1} is NB) then ({2} is S)",
            "if ({0} is PB) and ({1} is NM) then ({2} is S)",
            "if ({0} is PB) and ({1} is NS) then ({2} is S)",
            "if ({0} is PB) and ({1} is ZO) then ({2} is S)",
            "if ({0} is PB) and ({1} is PS) then ({2} is S)",
            "if ({0} is PB) and ({1} is PM) then ({2} is S)",
            "if ({0} is PB) and ({1} is PB) then ({2} is S)",
        };

        public static readonly string[] KdRules =
        {
            "if ({0} is NB) and ({1} is NB) then ({2} is B)",
            "if ({0} is NB) and ({1} is NM) then ({2} is B)",
            "if ({0} is NB) and ({1} is NS) then ({2} is B)",
            "if ({0} is NB) and ({1} is ZO) then ({2} is B)",
            "if ({0} is NB) and ({1} is PS) then ({2} is B)",
            "if ({0} is NB) and ({1} is PM) then ({2} is B)",
            "if ({0} is NB) and ({1} is PB) then ({2} is B)",

            "if ({0} is NM) and ({1} is NB) then ({2} is M)",
            "if ({0} is NM) and ({1} is NM) then ({2} is M)",
            "if ({0} is NM) and ({1} is NS) then ({2} is B)",
            "if ({0} is NM) and ({1} is ZO) then ({2} is B)",
            "if ({0} is NM) and ({1} is PS) then ({2} is B)",
            "if ({0} is NM) and ({1} is PM) then ({2} is M)",
            "if ({0} is NM) and ({1} is PB) then ({2} is M)",

            "if ({0} is NS) and ({1} is NB) then ({2} is S)",
            "if ({0} is NS) and ({1} is NM) then ({2} is M)",
            "if ({0} is NS) and ({1} is NS) then ({2} is M)",
            "if ({0} is NS) and ({1} is ZO) then ({2} is B)",
            "if ({0} is NS) and ({1} is PS) then ({2} is M)",
            "if ({0} is NS) and ({1} is PM) then ({2} is M)",
            "if ({0} is NS) and ({1} is PB) then ({2} is S)",

            "if ({0} is ZO) and ({1} is NB) then ({2} is ZO)",
            "if ({0} is ZO) and ({1} is NM) then ({2} is S)",
            "if ({0} is ZO) and ({1} is NS) then ({2} is M)",
            "if ({0} is ZO) and ({1} is ZO) then ({2} is B)",
            "if ({0} is ZO) and ({1} is PS) then ({2} is M)",
            "if ({0} is ZO) and ({1} is PM) then ({2} is S)",
            "if ({0} is ZO) and ({1} is PB) then ({2} is ZO)",

            "if ({0} is PS) and ({1} is NB) then ({2} is S)",
            "if ({0} is PS) and ({1} is NM) then ({2} is M)",
            "if ({0} is PS) and ({1} is NS) then ({2} is M)",
            "if ({0} is PS) and ({1} is ZO) then ({2} is B)",
            "if ({0} is PS) and ({1} is PS) then ({2} is M)",
            "if ({0} is PS) and ({1} is PM) then ({2} is M)",
            "if ({0} is PS) and ({1} is PB) then ({2} is S)",

            "if ({0} is PM) and ({1} is NB) then ({2} is M)",
            "if ({0} is PM) and ({1} is NM) then ({2} is M)",
            "if ({0} is PM) and ({1} is NS) then ({2} is B)",
            "if ({0} is PM) and ({1} is ZO) then ({2} is B)",
            "if ({0} is PM) and ({1} is PS) then ({2} is B)",
            "if ({0} is PM) and ({1} is PM) then ({2} is M)",
            "if ({0} is PM) and ({1} is PB) then ({2} is M)",

            "if ({0} is PB) and ({1} is NB) then ({2} is B)",
            "if ({0} is PB) and ({1} is NM) then ({2} is B)",
            "if ({0} is PB) and ({1} is NS) then ({2} is B)",
            "if ({0} is PB) and ({1} is ZO) then ({2} is B)",
            "if ({0} is PB) and ({1} is PS) then ({2} is B)",
            "if ({0} is PB) and ({1} is PM) then ({2} is B)",
            "if ({0} is PB) and ({1} is PB) then ({2} is B)",
        };
    }

    class SugenoZPid : ZPid
    {
        private double errorMin = -10, errorMax = 10;
        private double rateOfErrorMin = -10, rateOfErrorMax = 10;

        private string kdName = "kd";
        private string kiName = "ki";
        private string kpName = "kp";

        private double KPMin = 0, KPMax = 10;
        private double KIMin = 0, KIMax = 10;
        private double KDMin = 0, KDMax = 10;

        private SugenoFuzzySystem kPSugFuzSys;
        private SugenoFuzzySystem kISugFuzSys;
        private SugenoFuzzySystem kDSugFuzSys;

        public SugenoZPid()
        {
            kPSugFuzSys = MathUtils.CreateSugenoSysForKpKi(kpName, SugenoPidRules.KpRules);
            kISugFuzSys = MathUtils.CreateSugenoSysForKpKi(kiName, SugenoPidRules.KiRules);
            kDSugFuzSys = MathUtils.CreateSugenoSysForKd(kdName, SugenoPidRules.KdRules);
        }


        public override double Calc(QuadcopterParams quadcopter, params double[] args)
        {
            var curZ = args[0];
            var curPhi = args[1];
            var curTheta = args[2];
            var dt = args[3];
            var curVz = args[4];
            var curT = args[5];

            CurError = MathUtils.ConvertValUsingMinMax(quadcopter.Zdest - curZ, errorMin, errorMax);
            var rateOfError = MathUtils.ConvertValUsingMinMax(
                CalcRateOfError(curVz), 
                rateOfErrorMin, rateOfErrorMax);

            var normRateOfError = MathUtils.Normalize(rateOfError, rateOfErrorMin, rateOfErrorMax);
            var normError = MathUtils.Normalize(this.CurError, errorMin, errorMax);

            Kp = MathUtils.ComputeCoeffSugeno(kPSugFuzSys, normRateOfError, normError, kpName, KPMin, KPMax);
            Ki = MathUtils.ComputeCoeffSugeno(kISugFuzSys, normRateOfError, normError, kiName, KIMin, KIMax);
            Kd = MathUtils.ComputeCoeffSugeno(kDSugFuzSys, normRateOfError, normError, kdName, KDMin, KDMax);


            var res =  /*Math.Sign(curVz) * quadcopter.c_d * 0.5 * quadcopter.rho * curVz * curVz * quadcopter.S_z / (Math.Cos(curPhi)*Math.Cos(curTheta))*/
                + (
                    MathUtils.g + this.Kd * rateOfError + this.Ki * MathUtils.TrapezoidIntegration((t) => CurError, quadcopter.TMin, curT)
                    + this.Kp * CurError
                ) * quadcopter.Mt / (Math.Cos(curPhi)*Math.Cos(curTheta));
            return res;
        }
    }
}
