﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    public class Angle3d
    {
        private double phiRadians = 0;
        private double thetaRadians = 0;
        private double psiRadians = 0;

        public double PhiDegrees
        {
            get { return MathUtils.FromRadiansToDegrees(phiRadians); }
            set { phiRadians = MathUtils.FromDegreesToRadians(value); }
        }
        public double ThetaDegrees
        {
            get { return MathUtils.FromRadiansToDegrees(thetaRadians); }
            set { thetaRadians = MathUtils.FromDegreesToRadians(value); }
        }
        public double PsiDegrees
        {
            get { return MathUtils.FromRadiansToDegrees(psiRadians); }
            set { psiRadians = MathUtils.FromDegreesToRadians(value); }
        }

        public double PhiRadians
        {
            get { return phiRadians; }
            set { phiRadians = value; }
        }
        public double ThetaRadians
        {
            get { return thetaRadians; }
            set { thetaRadians = value; }
        }
        public double PsiRadians
        {
            get { return psiRadians; }
            set { psiRadians = value; }
        }
    }
}
