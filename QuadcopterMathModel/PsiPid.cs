﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    class PsiPid : AbstractQuadcopterPid
    {
        public override double Calc(QuadcopterParams quadcopter, params double[] args)
        {
            var curPsi = args[0];
            var dt = args[1];
            var curVpsi = args[2];
            CurError = quadcopter.DestAttitude.PsiRadians - curPsi;
            var curT = args[3];

            var rateOfError = CalcRateOfError(curVpsi);//(CurError - PrevError) / dt;

            var res = (
                Kd * rateOfError//(-curVpsi) 
                + this.Ki * MathUtils.TrapezoidIntegration((t) => CurError, quadcopter.TMin, curT)
                + Kp * CurError) * quadcopter.Jz;
            return res;
        }
    }
}
