﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    /// <summary>
    /// Абстрактный для ПИД-регулятора для квадрокоптера
    /// </summary>
    public abstract class AbstractQuadcopterPid
    {
        public double Kp { get; set; }
        public double Ki { get; set; }
        public double Kd { get; set; }
//        public QuadcopterParams Quadcopter { get; set; }
        public double PrevError { get; set; }
        public double CurError { get; set; }
        public abstract double Calc(QuadcopterParams quadcopter, params double[] args);

        public virtual double CalcRateOfError(params double[] args)
        {
            var v = args[0];
            return -v;
        }
    }

}
