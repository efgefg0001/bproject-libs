﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadcopterMathModel
{
    public class QuadcoptersFactory
    {
        public Motor CreateX2212KV980()
        {
            var motor = new Motor();
            motor.MinVoltage = 0;
            motor.MaxVoltage = 11.1;
            motor.Coeff = 102.6;
            motor.StartOmega = 300;
            return motor;
        }

        public TimeParams CreateTime()
        {
            return new TimeParams{TMax=10, H=1e-1};
        }

        public Func<double, double[], QuadcopterOdeSolver, double>[] CreateFuncs()
        {
            Func<double, double[], QuadcopterOdeSolver, double>[] funcs =
            {
                (tCur, xVec, solver) => xVec[1],
                (tCur, xVec, solver) => (Math.Cos(xVec[6]) * Math.Sin(xVec[8]) * Math.Cos(xVec[10]) + Math.Sin(xVec[6]) * Math.Sin(xVec[10])) * 
                    solver.Quadcopter.CalcF(solver.Quadcopter.Motor1.CurOmega, solver.Quadcopter.Motor2.CurOmega, solver.Quadcopter.Motor3.CurOmega, solver.Quadcopter.Motor4.CurOmega) 
                    / solver.Quadcopter.Mt//,
                    - MathUtils.Sign(xVec[1]) * solver.Quadcopter.c_d * solver.Quadcopter.rho * 0.5 * xVec[1] * xVec[1] * solver.Quadcopter.S_x/solver.Quadcopter.Mt,
                (tCur, xVec, solver) => xVec[3],
                (tCur, xVec, solver) => (Math.Cos(xVec[6]) * Math.Sin(xVec[8]) * Math.Sin(xVec[10]) - Math.Sin(xVec[6]) * Math.Cos(xVec[10])) * solver.Quadcopter.CalcF(
                    solver.Quadcopter.Motor1.CurOmega, solver.Quadcopter.Motor2.CurOmega, solver.Quadcopter.Motor3.CurOmega, solver.Quadcopter.Motor4.CurOmega) / solver.Quadcopter.Mt//,
                    - MathUtils.Sign(xVec[3]) * solver.Quadcopter.c_d * solver.Quadcopter.rho * 0.5 * xVec[3] * xVec[3] * solver.Quadcopter.S_y/solver.Quadcopter.Mt,
                (tCur, xVec, solver) => xVec[5],
                (tCur, xVec, solver) => {
                    var result = -MathUtils.g + Math.Cos(xVec[6]) * Math.Cos(xVec[8]) * solver.Quadcopter.CalcF(
                        solver.Quadcopter.Motor1.CurOmega, solver.Quadcopter.Motor2.CurOmega, solver.Quadcopter.Motor3.CurOmega, solver.Quadcopter.Motor4.CurOmega) / solver.Quadcopter.Mt;
                    var air = - MathUtils.Sign(xVec[5]) * solver.Quadcopter.c_d * solver.Quadcopter.rho * 0.5 * xVec[5] * xVec[5] * solver.Quadcopter.S_z/solver.Quadcopter.Mt;
                    result += air;
                    return result;
                },
                (tCur, xVec, solver) => xVec[7],
                (tCur, xVec, solver) => //xVec[9]*xVec[11]*(solver.Quadcopter.Jy - solver.Quadcopter.Jz) /solver.Quadcopter.Jx 
//                    + solver.Quadcopter.Jr/solver.Quadcopter.Jx * xVec[9] * solver.Quadcopter.CalcGyrosX(solver.CurOmega1, solver.CurOmega2, solver.CurOmega3, solver.CurOmega4) 
                    + /*solver.Quadcopter.l*/1 / solver.Quadcopter.Jx * solver.Quadcopter.CalcTauPhi(solver.Quadcopter.Motor2.CurOmega, solver.Quadcopter.Motor4.CurOmega),
                (tCur, xVec, solver) => xVec[9],
                (tCur, xVec, solver) => //xVec[7]*xVec[11]*(solver.Quadcopter.Jz - solver.Quadcopter.Jx) /solver.Quadcopter.Jy 
//                    + solver.Quadcopter.Jr/solver.Quadcopter.Jy * xVec[7] * solver.Quadcopter.CalcGyrosY(solver.CurOmega1, solver.CurOmega2, solver.CurOmega3, solver.CurOmega4)
                    + /*solver.Quadcopter.l*/1 / solver.Quadcopter.Jy * solver.Quadcopter.CalcTauTheta(solver.Quadcopter.Motor1.CurOmega, solver.Quadcopter.Motor3.CurOmega),
                (tCur, xVec, solver) => xVec[11],
                (tCur, xVec, solver) => //xVec[7]*xVec[9]*(solver.Quadcopter.Jx - solver.Quadcopter.Jy)/solver.Quadcopter.Jz
                    + 1 / solver.Quadcopter.Jz * solver.Quadcopter.CalcTauPsi(solver.Quadcopter.Motor1.CurOmega, solver.Quadcopter.Motor2.CurOmega, solver.Quadcopter.Motor3.CurOmega, solver.Quadcopter.Motor4.CurOmega),
            };
            return funcs;
        }

        public QuadcopterParams CreateStableQuadcopter()
        {
            var quadcopterParams = new QuadcopterParams();

            quadcopterParams.Zdest = 0;
            quadcopterParams.DestAttitude = new Angle3d { PhiDegrees = 0, ThetaDegrees = 0, PsiDegrees = 0 };

            var motor1 = CreateX2212KV980(); //motor1.StartOmega = 300;
            quadcopterParams.Motor1 = motor1;
            var motor2 = CreateX2212KV980(); //motor2.StartOmega = 300;
            quadcopterParams.Motor2 = motor2;
            var motor3 = CreateX2212KV980(); //motor3.StartOmega = 300;
            quadcopterParams.Motor3 = motor3;
            var motor4 = CreateX2212KV980(); //motor4.StartOmega = 300;
            quadcopterParams.Motor4 = motor4;

            quadcopterParams.S_x = 0.01;
            quadcopterParams.S_y = 0.03;
            quadcopterParams.S_z = 0.03;

            quadcopterParams.k = /*2.92e-6;*/1.4851e-5;
            quadcopterParams.b = 1.12e-7;//0.7426e-6;

            quadcopterParams.c_d = 0.4;
            quadcopterParams.rho = 1.2;

            quadcopterParams.l = 0.2;
            quadcopterParams.Ms = 0.6;
            quadcopterParams.Rs = 0.1;
            quadcopterParams.m = 0.1;

            // inertia
            quadcopterParams.Jx = 0.002353;
            quadcopterParams.Jy = 0.002353;
            quadcopterParams.Jz = 0.004706;
            quadcopterParams.Jr = 2e-5;

            quadcopterParams.Funcs = CreateFuncs();
            quadcopterParams.StartPosition = new Point3d { X=0, Y=0, Z=0 };
            quadcopterParams.StartVelocity = new Point3d { X = 0, Y = 0, Z = 0 };
            quadcopterParams.StartAttitude = new Angle3d { PhiDegrees = 0, ThetaDegrees = 0, PsiDegrees = 0 };
            quadcopterParams.StartAttitudeRate = new Angle3d { PhiDegrees = 0, ThetaDegrees = 0, PsiDegrees = 0 };

            quadcopterParams.TMin = 0;
            return quadcopterParams;
        }
        public QuadcopterParams CreateUnstableQuadcopter()
        {
            var quadcopterParams = new QuadcopterParams();

            quadcopterParams.Zdest = 5;
            quadcopterParams.DestAttitude = new Angle3d { PhiDegrees = 0, ThetaDegrees = 0, PsiDegrees = 0 };

            var motor1 = CreateX2212KV980(); //motor1.StartOmega = 300;
            quadcopterParams.Motor1 = motor1;
            var motor2 = CreateX2212KV980(); //motor2.StartOmega = 300;
            quadcopterParams.Motor2 = motor2;
            var motor3 = CreateX2212KV980(); //motor3.StartOmega = 300;
            quadcopterParams.Motor3 = motor3;
            var motor4 = CreateX2212KV980(); //motor4.StartOmega = 300;
            quadcopterParams.Motor4 = motor4;

            quadcopterParams.S_x = 0.01;
            quadcopterParams.S_y = 0.03;
            quadcopterParams.S_z = 0.03;

            quadcopterParams.k = /*2.92e-6;*/1.4851e-5;
            quadcopterParams.b = 1.12e-7;//0.7426e-6;

            quadcopterParams.c_d = 0.4;
            quadcopterParams.rho = 1.2;

            quadcopterParams.l = 0.2;
            quadcopterParams.Ms = 0.6;
            quadcopterParams.Rs = 0.1;
            quadcopterParams.m = 0.1;

            // inertia
            quadcopterParams.Jx = 0.002353;
            quadcopterParams.Jy = 0.002353;
            quadcopterParams.Jz = 0.004706;
            quadcopterParams.Jr = 2e-5;

            quadcopterParams.Funcs = CreateFuncs();
            quadcopterParams.StartPosition = new Point3d { X=0, Y=0, Z=1 };
            quadcopterParams.StartVelocity = new Point3d { X = 0, Y = 0, Z = 0 };
            quadcopterParams.StartAttitude = new Angle3d { PhiDegrees = 5, ThetaDegrees = 8, PsiDegrees = 10 };
            quadcopterParams.StartAttitudeRate = new Angle3d { PhiDegrees = 0, ThetaDegrees = 0, PsiDegrees = 0 };

            quadcopterParams.TMin = 0;
            return quadcopterParams;
        }

        public QuadcopterOdeSolver CreateFilledQuadcopterOdeSolverWithManualPids(QuadcopterParams quadcopter=null, TimeParams time=null)
        {
            var solver = new QuadcopterOdeSolver();

            if (quadcopter != null)
                solver.Quadcopter = quadcopter;
            else
                solver.Quadcopter = CreateStableQuadcopter();//CreateUnstableQuadcopter();
            if (time != null)
                solver.Time = time;
            else
                solver.Time = CreateTime();

            var zPid = new ZPid();
            //zPid.Kp = 1.5; zPid.Kd = 2.5;
            zPid.Kp = 3; zPid.Ki = 5; zPid.Kd = 4;
            solver.ZPid = zPid;

            var phiPid = new PhiPid();
            //phiPid.Kp = 6; phiPid.Kd = 1.75;
            phiPid.Kp = 6; phiPid.Ki = 5; phiPid.Kd = 3;
            solver.PhiPid = phiPid;

            var thetaPid = new ThetaPid();
            //thetaPid.Kp = 6; thetaPid.Kd = 1.75;
            thetaPid.Kp = 6; thetaPid.Ki = 5; thetaPid.Kd = 3;
            solver.ThetaPid = thetaPid;

            var psiPid = new PsiPid();
            //psiPid.Kp = 6; psiPid.Kd = 1.75;
            psiPid.Kp = 6; psiPid.Ki = 5; psiPid.Kd = 3;
            solver.PsiPid = psiPid;

            solver.Description = "Обычный ПИД";
                        
            return solver;
        }

        public QuadcopterOdeSolver CreateFilledOdeSolverForMamdaniFuzzyPids(QuadcopterParams quadcopter=null, TimeParams time=null)
        {
            var solver = new QuadcopterOdeSolver();
            if (quadcopter != null)
                solver.Quadcopter = quadcopter;
            else
                solver.Quadcopter = CreateStableQuadcopter();//CreateUnstableQuadcopter();
            if (time != null)
                solver.Time = time;
            else
                solver.Time = CreateTime();

            var zPid = new MamdaniZPid();
            zPid.Kp = 4; zPid.Ki = 4; zPid.Kd = 4;
            solver.ZPid = zPid;

            var phiPid = new MamdaniPhiPid();
            solver.PhiPid = phiPid;

            var thetaPid = new MamdaniThetaPid();
            solver.ThetaPid = thetaPid;

            var psiPid = new MamdaniPsiPid();
            solver.PsiPid = psiPid;
            
            solver.Description = "Фаззи-ПИД с алг. Мамдани";

            return solver;
        }

        public QuadcopterOdeSolver CreateFilledOdeSolverForSugenoFuzzyPids(QuadcopterParams quadcopter=null, TimeParams time=null)
        {
            var solver = new QuadcopterOdeSolver();
            if (quadcopter != null)
                solver.Quadcopter = quadcopter;
            else
                solver.Quadcopter = CreateStableQuadcopter();//CreateUnstableQuadcopter();
            if (time != null)
                solver.Time = time;
            else
                solver.Time = CreateTime();

            var zPid = new SugenoZPid();
            solver.ZPid = zPid;

            var phiPid = new SugenoPhiPid();
            solver.PhiPid = phiPid;

            var thetaPid = new SugenoThetaPid();
            solver.ThetaPid = thetaPid;

            var psiPid = new SugenoPsiPid();
            solver.PsiPid = psiPid;
            
            solver.Description = "Фаззи-ПИД с алг. Сугено";

            return solver;
        }

        public QuadcopterOdeSolver CreateTestSolver(TimeParams time)
        {
            var quadcopter = new QuadcopterParams();
            quadcopter.StartPosition = new Point3d {X=1 };
            quadcopter.StartVelocity = new Point3d { };
            quadcopter.Funcs = new Func<double, double[], QuadcopterOdeSolver, double>[] 
            {
                (t, xVec, slv) => -2 * t * xVec[0]
            };
            var solver = new QuadcopterOdeSolver();
            solver.Quadcopter = quadcopter;
            solver.Time = time;
            solver.UsePidRegulation = false;
            solver.Description = "Test";
            return solver;
        }
    }
}
